package com.hongyi.aceandroid.Activities

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.hongyi.aceandroid.Adapters.PurchasedItemAdapter
import com.hongyi.aceandroid.Databases.Helpers.UserViewModel
import com.hongyi.aceandroid.Entities.PurchasedItem
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.QRCodeUtil
import kotlinx.android.synthetic.main.activity_qr.*
import org.json.JSONArray

/**
 * Created by Hongyi on 2019-01-06.
 * @author Hongyi
 */

class QRActivity : AppCompatActivity(){
    private val purchasedItems = ArrayList<PurchasedItem>()
    private lateinit var purchasedItemAdapter: PurchasedItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr)
        val intent = intent
        val data = intent.getStringExtra("data")
        if (data != null){
            my_qr_code.text = "Please show QR to front desk"
            getPurchasedItems(data)
            val purchasedItemLinearLayoutManager = LinearLayoutManager(this)
            item_list.layoutManager = purchasedItemLinearLayoutManager
            purchasedItemAdapter = PurchasedItemAdapter()
            purchasedItemAdapter.setItemList(purchasedItems)
            item_list.adapter = purchasedItemAdapter
        }
//        val checkoutComplete = intent.getBooleanExtra("checkout_complete", false)
//        if (checkoutComplete){
//            val intent = Intent("CHECKOUT_COMPLETE")
//            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
//        }

        val userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        val currentUser = userViewModel.getCurrentUser()
        if (currentUser != null){
            val qrBitmap = QRCodeUtil.createQRCodeBitmap(currentUser.qrCode, 480, 480)
            image_qr.setImageBitmap(qrBitmap)
        } else{
            val qrBitmap = QRCodeUtil.createQRCodeBitmap("Welcome Hongyi", 480, 480)
            image_qr.setImageBitmap(qrBitmap)
        }
    }

    private fun getPurchasedItems(data: String){
        val items = JSONArray(data)
        for(i in 0 until items.length()){
            val item = items.getJSONObject(i)
            val name = item.getString("name")
            val count = item.getInt("count")
            purchasedItems.add(PurchasedItem(name, count))
        }
    }
}