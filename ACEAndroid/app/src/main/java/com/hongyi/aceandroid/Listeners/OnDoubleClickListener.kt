package com.hongyi.aceandroid.Listeners

import android.view.View

/**
 * Created by Hongyi on 2019-01-24.
 * @author Hongyi
 */

abstract class OnDoubleClickListener : View.OnClickListener {
    private val DOUBLE_TIME: Long = 300
    private var lastClickTime: Long = 0

    override fun onClick(p0: View?) {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastClickTime < DOUBLE_TIME){
            onDoubleClick(p0)
        }
        lastClickTime = currentTime
    }
    abstract fun onDoubleClick(v: View?)
}