package com.hongyi.aceandroid.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.hongyi.aceandroid.Fragments.HomeFragment
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.hourMap

/**
 * Created by Hongyi on 2019-02-01.
 * @author Hongyi
 */

class TimeAdapter(private val context: Context, private val onSwitchFragmentListener: HomeFragment.OnSwitchFragmentListener): RecyclerView.Adapter<TimeAdapter.ViewHolder>() {
    private val timeList = ArrayList<Int>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TimeAdapter.ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_time_button, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return timeList.size
    }

    override fun onBindViewHolder(p0: TimeAdapter.ViewHolder, p1: Int) {
        p0.timeButton.text = hourMap[timeList[p1]]
        p0.timeButton.id = timeList[p1]
        p0.timeButton.setOnClickListener {
            onSwitchFragmentListener.switchToBooking(p0.timeButton.id)
            Log.e("1234567890",  p0.timeButton.id.toString())
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var timeButton: Button = itemView.findViewById(R.id.time_btn)
    }

    fun setTimeList(timeList: ArrayList<Int>){
        this.timeList.clear()
        this.timeList.addAll(timeList)
    }
}