package com.hongyi.aceandroid.Utils

import android.graphics.Bitmap
import android.graphics.Color
import android.support.annotation.ColorInt
import android.text.TextUtils
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import java.util.Hashtable

/**
 * Created by Hongyi on 2019-01-11.
 * @author Hongyi
 */

object QRCodeUtil {

    @JvmOverloads
    fun createQRCodeBitmap(
        content: String, width: Int, height: Int,
        character_set: String? = "UTF-8", error_correction: String? = "H", margin: String? = "2",
        @ColorInt color_black: Int = Color.BLACK, @ColorInt color_white: Int = Color.WHITE
    ): Bitmap? {

        if (TextUtils.isEmpty(content)) {
            return null
        }

        if (width < 0 || height < 0) {
            return null
        }

        try {
            val hints = Hashtable<EncodeHintType, String>()

            if (!TextUtils.isEmpty(character_set)) {
                hints[EncodeHintType.CHARACTER_SET] = character_set
            }

            if (!TextUtils.isEmpty(error_correction)) {
                hints[EncodeHintType.ERROR_CORRECTION] = error_correction
            }

            if (!TextUtils.isEmpty(margin)) {
                hints[EncodeHintType.MARGIN] = margin
            }
            val bitMatrix = QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints)

            val pixels = IntArray(width * height)
            for (y in 0 until height) {
                for (x in 0 until width) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * width + x] = color_black
                    } else {
                        pixels[y * width + x] = color_white
                    }
                }
            }

            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
            return bitmap
        } catch (e: WriterException) {
            e.printStackTrace()
        }

        return null
    }
}