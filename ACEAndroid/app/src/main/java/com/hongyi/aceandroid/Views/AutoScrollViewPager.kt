package com.hongyi.aceandroid.Views

import android.support.v4.view.PagerAdapter
import android.widget.FrameLayout
import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Message
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import android.util.AttributeSet
import android.view.View
import com.hongyi.aceandroid.R

/**
 * Created by Hongyi on 2019-01-24.
 * @author Hongyi
 */

class AutoScrollViewPager : FrameLayout {
    private var viewPager: ViewPager? = null
    private var adapter: PagerAdapter? = null
//    private var linearLayout: LinearLayout? = null
    private var pageMargin = 0
    private var oldPosition = 0
    private var currentIndex = 1
    private val time: Long = 3000
    private var autoPlay = true

    @SuppressLint("HandlerLeak")
    private val handler = object : Handler() {
        override fun handleMessage(msg: Message?) {
            play()
        }
    }
    private val runnable = Runnable { viewPager!!.currentItem = ++currentIndex }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init{
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        clipChildren = false
        pageMargin = resources.getDimensionPixelSize(R.dimen.page_margin)

        viewPager = ViewPager(context)
        val layoutParams = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        layoutParams.leftMargin = pageMargin * 2
        layoutParams.rightMargin = pageMargin * 2
        viewPager!!.layoutParams = layoutParams
        addView(viewPager)

//        linearLayout = LinearLayout(context)
//        val params = FrameLayout.LayoutParams(
//            ViewGroup.LayoutParams.MATCH_PARENT,
//            ViewGroup.LayoutParams.WRAP_CONTENT
//        )
//        params.gravity = Gravity.BOTTOM
//        linearLayout!!.gravity = Gravity.CENTER
//        linearLayout!!.orientation = LinearLayout.HORIZONTAL
//        addView(linearLayout, params)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        play()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        cancel()
    }

    private fun initViewPager() {
        if (adapter == null) {
            return
        }
        viewPager!!.currentItem = currentIndex
        viewPager!!.offscreenPageLimit = 1
        viewPager!!.pageMargin = pageMargin
        //        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                currentIndex = when (position) {
                    0 -> adapter!!.count - 2
                    adapter!!.count - 1 -> 1
                    else -> position
                }
//                linearLayout!!.getChildAt(oldPosition).isEnabled = false
//                linearLayout!!.getChildAt(currentIndex - 1).isEnabled = true
                oldPosition = currentIndex - 1
            }

            override fun onPageScrollStateChanged(state: Int) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewPager!!.setCurrentItem(currentIndex, false)
                    play()
                } else if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    cancel()
                }
            }
        })
//        setIndicatorDot()
//        linearLayout!!.getChildAt(0).isEnabled = true
    }

    fun play() {
        if (autoPlay) {
            handler.postDelayed(runnable, time)
        } else {
            handler.removeCallbacks(runnable)
        }
    }

    fun cancel() {
        handler.removeCallbacks(runnable)
    }

//    private fun setIndicatorDot() {
//        for (i in 0 until adapter!!.count - 2) {
//            val v = View(context)
//            v.setBackgroundResource(R.drawable.dot_view_pager)
//            v.setEnabled(false)
//            val params = LinearLayout.LayoutParams(20, 20)
//            if (i != 0)
//                params.leftMargin = 20
//            params.bottomMargin = 20
//            v.setLayoutParams(params)
//            linearLayout!!.addView(v)
//        }
//    }

    fun setAdapter(adapter: PagerAdapter) {
        this.adapter = adapter
        viewPager!!.adapter = adapter
        initViewPager()
    }

    fun setAutoPlay(autoPlay: Boolean) {
        this.autoPlay = autoPlay
        if (!autoPlay) {
            handler.removeCallbacks(runnable)
        }
    }
}