package com.hongyi.aceandroid.Utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.support.media.ExifInterface
import android.util.Base64
import java.io.ByteArrayOutputStream

/**
 * Created by Hongyi on 2019-01-22.
 * @author Hongyi
 */

object ImageUtil {
    fun rotateImage(path : String, bitmap : Bitmap) : Bitmap {
        val exifInterface = ExifInterface(path)
        val orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        lateinit var rotatedBitmap : Bitmap
        val matrix = Matrix()
        when(orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> {
                matrix.postRotate(90F)
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            }
            ExifInterface.ORIENTATION_ROTATE_180 -> {
                matrix.postRotate(180F)
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> {
                matrix.postRotate(270F)
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            }
            else ->
                rotatedBitmap = bitmap
        }
        return rotatedBitmap
    }

    fun bitmap2String(bitmap: Bitmap) : String {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray = stream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    fun string2Bitmap(s: String) : Bitmap? {
        var bitmap: Bitmap? = null
        return try {
            val bitmapArray = Base64.decode(s, Base64.DEFAULT)
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.size)
            bitmap
        } catch (e: Exception) {
            e.printStackTrace()
            bitmap
        }

//        return BitmapFactory.decodeByteArray(s.toByteArray(UTF_8), 0, s.length)
    }

    fun px2dp(context: Context, px: Float): Float {
        return px / context.resources.displayMetrics.density
    }

    fun dp2px(context: Context, dp: Float): Float {
        return dp * context.resources.displayMetrics.density
    }
}