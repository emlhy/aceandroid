package com.hongyi.aceandroid.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hongyi.aceandroid.R

/**
 * Created by Hongyi on 2019-01-03.
 * @author Hongyi
 */

class ClubFragment : Fragment() {
    companion object {
        fun newInstance(): ClubFragment {
            return ClubFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_club, container, false)
        return rootView
    }
}