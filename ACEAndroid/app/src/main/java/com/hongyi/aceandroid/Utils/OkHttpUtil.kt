package com.hongyi.aceandroid.Utils

import okhttp3.*
import org.json.JSONObject
import okhttp3.RequestBody
import okhttp3.MultipartBody
import java.io.File
import java.util.concurrent.TimeUnit


/**
 * Created by Hongyi on 2019-01-18.
 * @author Hongyi
 */

const val HTTP_REQUEST_METHOD_POST = "POST"
const val ACE_URL = "http://35.182.55.43:3000"
//const val ACE_URL = "http://40.87.7.47:3000"
const val REGISTER_PREFIX_URL = "/register"
const val LOGIN_PREFIX_URL = "/login"
const val USER_INFO_PREFIX_URL = "/user/info"
const val WALLET_INFO_PREFIX_URL = "/walletInfo"
const val MEMBERSHIP_TYPE_PREFIX_URL = "/membershipType"
//const val TOKEN_OPTIONS_PREFIX_URL = "/tokenOptions"
const val TOKEN_CARD_TYPE_PREFIX_URL = "/tokenCardTypes"
const val BALANCE_OPTION_PREFIX_URL = "/balanceOption"
const val PURCHASE_MEMBERSHIP_PREFIX_URL = "/purchaseMembership"
const val AVAILABILITY_PREFIX_URL = "/availability"
const val PURCHASE_BOOKING_PREFIX_URL = "/purchaseBooking"
const val BOOKING_HISTORY_PREFIX_URL = "/bookinghistory"
const val STORE_ITEMS_PREFIX_URL = "/storeItems"
const val PURCHASE_PREFIX_URL = "/purchase"
const val TOKEN_CARD_PREFIX_URL = "/tokencard"
const val STORE_TYPES_PREFIX_URL = "/storeTypes"
const val CANCEL_PENDING_BOOKING_PREFIX_URL = "/cancelPendingBooking"
const val UPLOAD_AVATAR_PREFIX_URL = "/uploadAvatar"
const val UPDATE_AVATAR_PREFIX_URL = "/updateAvatar"
const val USER_UPDATE_PREFIX_URL = "/user/update"

object OkHttpUtil {
    fun sendHttpRequestUseOkHttp(url: String, method: String, headerParamsMap: Map<String, String>?, postParams: JSONObject?, callback: Callback) {
        val okHttpClient = OkHttpClient()
        val builder = Request.Builder()
        builder.url(url)

        if (headerParamsMap != null) {
            val headers = Headers.of(headerParamsMap.toMutableMap())
            builder.headers(headers)
        }
        if (postParams != null) {
            val JSON = MediaType.parse("application/json; charset=utf-8")
            if (HTTP_REQUEST_METHOD_POST == method) {
//            val formBodyBuilder = FormBody.Builder()
//
//            if (postParamsMap != null) {
//                val keySet = postParamsMap.keys
//                val it = keySet.iterator()
//                while (it.hasNext()) {
//                    val key = it.next()
//                    val value = postParamsMap[key]
//                    formBodyBuilder.add(key, value!!)
//                }
//            }
//            val formBody = formBodyBuilder.build()
//            builder.post(formBody)
//                val parameter = JSONObject(postParamsMap)
                val requestBody = RequestBody.create(JSON, postParams.toString())
                builder.post(requestBody)
            }
        }

        val request = builder.build()
        val call = okHttpClient.newCall(request)
        call.enqueue(callback)
    }

    fun uploadImage(url: String, file: File, headerParamsMap: Map<String, String>?, callback: Callback) {
        val okHttpClient = OkHttpClient()
        val builder = Request.Builder()
        builder.url(url)

        if (headerParamsMap != null) {
            val headers = Headers.of(headerParamsMap.toMutableMap())
            builder.headers(headers)
        }

        val requestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
            .addFormDataPart(
                "image", file.name,
                RequestBody.create(MediaType.parse("image/jpeg"), file)
            )
            .build()
        builder.post(requestBody)

        val request = builder.build()

        val call = okHttpClient.newCall(request)
        call.enqueue(callback)
    }

}