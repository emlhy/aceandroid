package com.hongyi.aceandroid.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

/**
 * Created by Hongyi on 2019-01-03.
 * @author Hongyi
 */

class ViewpagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {
    var fragmentList = ArrayList<Fragment>()

    override fun getItem(p0: Int): Fragment {
        return fragmentList[p0]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    fun addFragment(fragment: Fragment){
        fragmentList.add(fragment)
    }

}