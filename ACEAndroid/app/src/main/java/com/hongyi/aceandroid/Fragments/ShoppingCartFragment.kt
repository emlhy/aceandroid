package com.hongyi.aceandroid.Fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.currentUser
import com.hongyi.aceandroid.Activities.QRActivity
import com.hongyi.aceandroid.Activities.SignInActivity
import com.hongyi.aceandroid.Adapters.ShoppingCartAdapter
import com.hongyi.aceandroid.Adapters.ShoppingCartAdapter.Companion.amountArray
import com.hongyi.aceandroid.Adapters.ShoppingCartAdapter.Companion.cartList
import com.hongyi.aceandroid.Adapters.ShoppingCartAdapter.Companion.subtotalArray
import com.hongyi.aceandroid.Entities.Commodity
//import com.hongyi.aceandroid.Fragments.RootFragment.FragmentState.isStoreFragment
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.ACE_URL
import com.hongyi.aceandroid.Utils.OkHttpUtil
import com.hongyi.aceandroid.Utils.PURCHASE_PREFIX_URL
import kotlinx.android.synthetic.main.fragment_checkout.*
import kotlinx.android.synthetic.main.fragment_checkout.view.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

/**
 * Created by Hongyi on 2019-01-25.
 * @author Hongyi
 */

class ShoppingCartFragment : Fragment() {
    lateinit var shoppingCartAdapter: ShoppingCartAdapter
    companion object {
        fun newInstance(): ShoppingCartFragment {
            return ShoppingCartFragment()
        }

        var checkoutCompleteForStore = false
        var checkoutCompleteForWallet = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_checkout, container, false)
//        isStoreFragment = false
        val bundle = arguments
        val selectedCommodities = bundle!!.getParcelableArrayList<Commodity>("selected_item")
        val cartList = ArrayList<Commodity>()
        selectedCommodities!!.forEach{
            cartList.add(it)
        }
        val linearLayoutManager = LinearLayoutManager(activity)
        rootView.cart_list.layoutManager = linearLayoutManager
        rootView.cart_list.setHasFixedSize(true)
//        val cartList = arrayOf(
//            R.drawable.radio_round_corner_mint,
//            R.drawable.radio_round_corner_white,
//            R.drawable.radio_round_corner_mint,
//            R.drawable.radio_round_corner_white,
//            R.drawable.radio_round_corner_mint,
//            R.drawable.radio_round_corner_white,
//            R.drawable.radio_round_corner_mint,
//            R.drawable.radio_round_corner_white,
//            R.drawable.radio_round_corner_mint,
//            R.drawable.radio_round_corner_white,
//            R.drawable.radio_round_corner_mint,
//            R.drawable.radio_round_corner_white
//        )
        shoppingCartAdapter = ShoppingCartAdapter(activity as Context, rootView.checkout_btn)
        shoppingCartAdapter.setCartList(cartList)
        rootView.cart_list.adapter = shoppingCartAdapter
        rootView.checkout_btn.setOnClickListener {
            if (ShoppingCartAdapter.cartList.size != 0) {
                if (currentUser != null) {
                    val builder = AlertDialog.Builder(activity as Context)
                    builder.setTitle("Purchase")
                    builder.setMessage("Are you sure?")
                    builder.setCancelable(true)
                    builder.setPositiveButton("YES") { dialog, which ->
                        purchase()
                    }
                    builder.setNegativeButton("No") { dialog, which -> }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                } else {
                    val view: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(view, "Please login before you purchase", Snackbar.LENGTH_LONG)
                        .setAction("OK") {
                            val intent = Intent(context as Activity, SignInActivity::class.java)
                            startActivity(intent)
                            (context as Activity).finish()
                        }.show()
                }
            }
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (subtotalArray.size != 0){
            var total = 0.0
            for ((key, value) in subtotalArray) {
                total += subtotalArray[key]!!
            }
            checkout_btn.text = "Total $$total"
        } else {
            checkout_btn.text = "Total"
        }
    }

    private fun purchase(){
        val header = mapOf("Content-Type" to "application/jason", "auth-token" to currentUser!!.token)
        val body = JSONObject()
        val purchases = JSONArray()
        for (i in 0 until cartList.size){
            val item = JSONObject()
            item.put("itemUUID", cartList[i].uuid)
            item.put("count", amountArray[cartList[i].uuid])
            purchases.put(item)
        }
        body.put("purchases", purchases)
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + PURCHASE_PREFIX_URL, "POST", header, body,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getString("data")
                        cartList.clear()
                        amountArray.clear()
                        subtotalArray.clear()
                        activity?.runOnUiThread {
                            shoppingCartAdapter.notifyDataSetChanged()
                        }
                        checkoutCompleteForStore = true
                        checkoutCompleteForWallet = true
                        val intent = Intent(activity, QRActivity::class.java)
                        intent.putExtra("data", data)
                        startActivity(intent)
//                        activity!!.supportFragmentManager.popBackStack()
//                        val message = jsonObjectResponse.getString("msg")
//                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
//                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }
}