package com.hongyi.aceandroid.Fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.currentUser
import com.hongyi.aceandroid.Adapters.DetailCardAdapter
import com.hongyi.aceandroid.Fragments.AccountFragment.Companion.index
import com.hongyi.aceandroid.Fragments.ShoppingCartFragment.Companion.checkoutCompleteForWallet
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.*
import kotlinx.android.synthetic.main.fragment_wallet.*
import kotlinx.android.synthetic.main.view_balance.*
import kotlinx.android.synthetic.main.view_balance.view.*
import kotlinx.android.synthetic.main.view_membership.*
import kotlinx.android.synthetic.main.view_membership.view.*
import kotlinx.android.synthetic.main.view_token.*
import kotlinx.android.synthetic.main.view_token.view.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

/**
 * Created by Hongyi on 2019-01-29.
 * @author Hongyi
 */

class WalletFragment : Fragment() {
//    lateinit var switchToWallet: SwitchToWallet
    lateinit var walletHandler: Handler
    lateinit var detailHandler: Handler
    private lateinit var membershipAdapter: DetailCardAdapter
    private lateinit var tokenAdapter: DetailCardAdapter
    private lateinit var balanceAdapter: DetailCardAdapter

    companion object {
        fun newInstance(): WalletFragment {
            return WalletFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        Log.e("1234567890", "WalletFragment: onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_wallet, container, false)
//        Log.e("1234567890", "WalletFragment: onCreateView")

        if (currentUser != null){
            getWalletInfo()
        }

        val membershipLinearLayoutManager = LinearLayoutManager(activity)
        val tokenLinearLayoutManager = LinearLayoutManager(activity)
        val balanceLinearLayoutManager = LinearLayoutManager(activity)
        rootView.membership_options.layoutManager = membershipLinearLayoutManager
        rootView.token_options.layoutManager = tokenLinearLayoutManager
        rootView.balance_options.layoutManager = balanceLinearLayoutManager
        getCardDetail()

        rootView.membership_tab.setOnClickListener {
            rootView.membership_expandable_layout.toggle()
        }
        rootView.token_tab.setOnClickListener {
            rootView.token_expandable_layout.toggle()
        }
        rootView.balance_tab.setOnClickListener {
            rootView.balance_expandable_layout.toggle()
        }
        return rootView
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        switchToWallet = (parentFragment as AccountFragment) as SwitchToWallet
//    }
//
//    override fun onResume() {
//        super.onResume()
//        switchToWallet.switchToWallet()
//    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        Log.e("1234567890", "WalletFragment: onViewCreated")
//        Log.e("1234567890", "MemberShip: " + membership_expandable_layout.isExpanded.toString())
//        Log.e("1234567890", "Top Up: " + top_up_expandable_layout.isExpanded.toString())
//        Log.e("1234567890", "Balance: " + balance_expandable_layout.isExpanded.toString())
        val margin = ImageUtil.dp2px(activity!!, 8f).toInt()
        if (index != -1) {
            when (index) {
                0 -> {
                    token_expandable_layout.expand()
                    balance_expandable_layout.expand()
                    token_expandable_layout.collapse()
                    balance_expandable_layout.collapse()
                    membership_expandable_layout.expand()
                }
                1 -> {
                    membership_expandable_layout.expand()
                    balance_expandable_layout.expand()
                    membership_expandable_layout.collapse()
                    balance_expandable_layout.collapse()
                    token_expandable_layout.expand()
                    wallet_nested_scroll_view.postDelayed(
                        {
                            val scrollY = membership_tab.height + margin
                            wallet_nested_scroll_view.smoothScrollTo(0, scrollY)
                        },
                        200
                    )
                }
                2 -> {
                    membership_expandable_layout.expand()
                    token_expandable_layout.expand()
                    membership_expandable_layout.collapse()
                    token_expandable_layout.collapse()
                    balance_expandable_layout.expand()
                    wallet_nested_scroll_view.postDelayed(
                        {
                            val scrollY = membership_tab.height + token_tab.height + margin * 2
                            wallet_nested_scroll_view.smoothScrollTo(0, scrollY)
                        },
                        200
                    )
                }
            }
            (parentFragment as AccountFragment).switchToWalletFragment()
            index = -1
        }
    }

    override fun onResume() {
        super.onResume()
        if (checkoutCompleteForWallet) {
            getWalletInfo()
            checkoutCompleteForWallet = false
        }
    }

    fun getWalletInfo(){
        walletHandler = Handler(Handler.Callback {
            if (it != null) {
                val result = it.obj as JSONObject
                if (!result.isNull("wallet")) {
                    val walletObject = result.getJSONObject("wallet")
//                    val token = walletObject.getInt("token")
                    val balance = walletObject.getInt("balance").toFloat()
//                    if (!result.isNull("validMembership")) {
//                        val membershipObject = result.getJSONObject("validMembership")
//                        val expireDate = membershipObject.getString("expireDate").substring(0, 10)
//                        expire_date.text = expireDate
//                    }
//                    token_left.text = token.toString()
                    balance_amount.text = balance.toString()
                }
                if (!result.isNull("membership")) {
                    val membershipObject = result.getJSONObject("membership")
                    val expireDate = membershipObject.getString("expireDate").substring(0, 10)
                    expire_date.text = expireDate
                }
                if (!result.isNull("tokenCard")) {
                    val tokenCardObject = result.getJSONObject("tokenCard")
                    val tokenCardType = tokenCardObject.getJSONObject("tokenCardType")
                    val tokens = tokenCardType.getInt("tokens")
                    token_left.text = tokens.toString()
                }
            }
            true
        })
        val header = mapOf("Content-Type" to "application/jason", "auth-token" to currentUser!!.token)
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + WALLET_INFO_PREFIX_URL, "GET", header, null,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getJSONObject("data")
                        val message = Message()
                        message.obj = data
                        walletHandler.sendMessage(message)
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }

    private fun getCardDetail(){
        detailHandler = Handler(Handler.Callback {
            if (it != null) {
                val result = it.obj as Map<*, *>
                if (result.containsKey(1)) {
                    membershipAdapter = DetailCardAdapter(activity as Context, this, result[1] as JSONArray, 1)
                    membership_options.adapter = membershipAdapter
                }
                if (result.containsKey(2)) {
                    tokenAdapter = DetailCardAdapter(activity as Context, this, result[2] as JSONArray, 2)
                    token_options.adapter = tokenAdapter
                }
                if (result.containsKey(3)) {
                    balanceAdapter = DetailCardAdapter(activity as Context, this, result[3] as JSONArray, 3)
                    balance_options.adapter = balanceAdapter
                }
            }
            true
        })
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + MEMBERSHIP_TYPE_PREFIX_URL, "GET", null, null,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getJSONArray("data")
                        val dataMap = mapOf(1 to data)
                        val message = Message()
                        message.obj = dataMap
                        detailHandler.sendMessage(message)
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + TOKEN_CARD_TYPE_PREFIX_URL, "GET", null, null,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getJSONArray("data")
                        val dataMap = mapOf(2 to data)
                        val message = Message()
                        message.obj = dataMap
                        detailHandler.sendMessage(message)
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + BALANCE_OPTION_PREFIX_URL, "GET", null, null,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getJSONArray("data")
                        val dataMap = mapOf(3 to data)
                        val message = Message()
                        message.obj = dataMap
                        detailHandler.sendMessage(message)
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }

//    override fun onResume() {
//        super.onResume()
//        Log.e("1234567890", "WalletFragment: onResume")
//    }
//
//    override fun onPause() {
//        super.onPause()
//        Log.e("1234567890", "WalletFragment: onPause")
//        index = -1
//    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//        Log.e("1234567890", "WalletFragment: onDestroyView")
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        Log.e("1234567890", "WalletFragment: onDestroy")
//    }

//    fun scrollToView() {
//        when (index) {
//            0 -> {
//                top_up_expandable_layout.collapse()
//                balance_expandable_layout.collapse()
//                membership_expandable_layout.expand()
//            }
//            1 -> {
//                membership_expandable_layout.collapse()
//                balance_expandable_layout.collapse()
//                top_up_expandable_layout.expand()
//                wallet_nested_scroll_view.postDelayed(
//                    {
//                        val scrollY = membership_tab.height
//                        wallet_nested_scroll_view.smoothScrollTo(0, scrollY)
//                    },
//                    500
//                )
//            }
//            2 -> {
//                membership_expandable_layout.collapse()
//                top_up_expandable_layout.collapse()
//                balance_expandable_layout.expand()
//                wallet_nested_scroll_view.postDelayed(
//                    {
//                        val scrollY = membership_tab.height + top_up_tab.height
//                        wallet_nested_scroll_view.smoothScrollTo(0, scrollY)
//                    },
//                    500
//                )
//            }
//        }
//    }
//
//    interface SwitchToWallet {
//        fun switchToWallet()
//    }
}