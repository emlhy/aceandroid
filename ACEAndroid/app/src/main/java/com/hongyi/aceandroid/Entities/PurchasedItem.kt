package com.hongyi.aceandroid.Entities

/**
 * Created by Hongyi on 2019-04-22.
 * @author Hongyi
 */

data class PurchasedItem(var name: String, var count: Int) {
}