package com.hongyi.aceandroid.Adapters

import android.app.Activity
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.hongyi.aceandroid.Entities.Commodity
import com.hongyi.aceandroid.Listeners.OnDoubleClickListener
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.os.AsyncTask
import com.hongyi.aceandroid.R
import com.squareup.picasso.Picasso
import java.net.URL
import java.lang.ref.WeakReference

/**
 * Created by Hongyi on 2019-01-24.
 * @author Hongyi
 */

class StoreAdapter(private val context: Context, private val button: Button) : RecyclerView.Adapter<StoreAdapter.ViewHolder>() {
    companion object {
        val commodityList = ArrayList<Commodity>()
        val selectedCommodities = ArrayList<Commodity>()
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): StoreAdapter.ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_commodity_image, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return commodityList.size
    }

    override fun onBindViewHolder(p0: StoreAdapter.ViewHolder, p1: Int) {
//        LoadImage(context.applicationContext, p0, commodityList[p1]).execute()
        Picasso.get().load(commodityList[p1].imageURL).into(p0.image)

//        p0.image.setImageDrawable(context.getDrawable(commodityList[p1]))
//        p0.name.setText(p1.toString())
//        p0.price.setText("$"+ p1.toString())
        p0.name.text = commodityList[p1].name
        p0.price.text = "$" + commodityList[p1].price
        if (selectedCommodities.contains(commodityList[p1])){
            p0.checkMark.visibility = View.VISIBLE
        } else {
            p0.checkMark.visibility = View.INVISIBLE
        }
        p0.image.setOnClickListener(object : OnDoubleClickListener(){
            override fun onDoubleClick(v: View?) {
                Log.e("1234567890", p1.toString() + " " + p0.image.id.toString())
                val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                if (!selectedCommodities.contains(commodityList[p1])) {
                    selectedCommodities.add(commodityList[p1])
                    p0.checkMark.visibility = View.VISIBLE
                    if (!selectedCommodities.isEmpty()){
                        button.text = "Shopping Cart(" + selectedCommodities.size.toString() + ")"
                    } else {
                        button.text = "Shopping Cart"
                    }
                    Snackbar.make(rootView, "An item has been added into your shopping cart", Snackbar.LENGTH_LONG)
//                        .setAction(
//                            "Undo"
//                        ) {
//                            selectedCommodities.remove(commodityList[p1])
//                            p0.checkMark.visibility = View.INVISIBLE
//                            if (!selectedCommodities.isEmpty()){
//                                button.setText("Shopping Cart(" + selectedCommodities.size.toString() + ")")
//                            } else {
//                                button.setText("Shopping Cart")
//                            }
//                            Snackbar.make(rootView, "The item has been removed from your shopping cart", Snackbar.LENGTH_SHORT).show()
//                        }
                        .addCallback(object : Snackbar.Callback() {

                        }).show()
                } else {
                    Snackbar.make(rootView, "The item is already in your shopping cart", Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView = itemView.findViewById(R.id.commodity_image)
        var name: TextView = itemView.findViewById(R.id.commodity_name)
        var price: TextView = itemView.findViewById(R.id.commodity_price)
        var checkMark: ImageView = itemView.findViewById(R.id.check_mark)
    }

    class LoadImage(val context: Context, private val viewHolder: ViewHolder, private val commodity: Commodity) :  AsyncTask<Void, Void, Void>() {
        private var contextRef: WeakReference<Context> = WeakReference(context)
        private var bitmap: Bitmap? = null
        override fun doInBackground(vararg params: Void): Void? {
            try {
                val inputStream = URL(commodity.imageURL).openStream()
                bitmap = BitmapFactory.decodeStream(inputStream)
            } catch (e: Exception) {

            }
            return null
        }

        override fun onPostExecute(result: Void) {
            if (bitmap != null) {
                viewHolder.image.setImageBitmap(bitmap)
            } else {
                //Bitmap.Config conf = Bitmap.Config.ARGB_8888;
                //Bitmap bmp = Bitmap.createBitmap(500, 500, conf);
                val icon = BitmapFactory.decodeResource(contextRef.get()?.resources, R.mipmap.ic_launcher)
                viewHolder.image.setImageBitmap(icon)
            }
        }
    }

    fun setCommodityList(commodityList: ArrayList<Commodity>){
        Companion.commodityList.clear()
        Companion.commodityList.addAll(commodityList)
    }
}