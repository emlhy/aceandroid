package com.hongyi.aceandroid.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hongyi.aceandroid.R

/**
 * Created by Hongyi on 2019-01-25.
 * @author Hongyi
 */

class RootFragment : Fragment(){
//    object FragmentState{
//        var isStoreFragment = true
//    }
    companion object {
        fun newInstance(): RootFragment {
            return RootFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_root, container, false)
        val fragmentTransaction = activity!!.supportFragmentManager!!.beginTransaction()
//        if (isStoreFragment) {
            fragmentTransaction.replace(R.id.root_frame, StoreFragment())
//        } else {
//            fragmentTransaction.replace(R.id.root_frame, ShoppingCartFragment())
//        }
        fragmentTransaction.commit()
        return rootView
    }
}