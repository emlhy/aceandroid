package com.hongyi.aceandroid.Adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.hongyi.aceandroid.R

/**
 * Created by Hongyi on 2019-01-24.
 * @author Hongyi
 */

class AutoScrollViewAdapter(private val context: Context) : PagerAdapter() {
    private val resIds = arrayOf(
//        R.drawable.ic_account,
//        R.drawable.ic_home,
//        R.drawable.ic_calendar,
//        R.drawable.ic_cart,
//        R.drawable.ic_ping_pong,
//        R.drawable.ic_account,
//        R.drawable.ic_home
        R.drawable.radio_round_corner_white,
        R.drawable.radio_round_corner_mint,
        R.drawable.radio_round_corner_white,
        R.drawable.radio_round_corner_mint
    )

    override fun isViewFromObject(p0: View, p1: Any): Boolean {
        return p0 == p1
    }

    override fun getCount(): Int {
        return resIds.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = View.inflate(context, R.layout.item_gallery_image, null)
        val imageView = view.findViewById(R.id.gallery_image) as ImageView
        imageView.setImageDrawable(context.getDrawable(resIds[position]))
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}