package com.hongyi.aceandroid.Utils

import okhttp3.*
import org.json.JSONObject
import java.io.File

/**
 * Created by Hongyi on 2019-01-23.
 * @author Hongyi
 */

class OkHttpTest(){
    private var multipartBody: MultipartBody.Builder = MultipartBody.Builder()
    private var okHttpClient: OkHttpClient

    init {
        this.multipartBody.setType(MultipartBody.FORM)
        this.okHttpClient = OkHttpClient()
    }

    fun addString(postParamsMap: Map<String, String>) {
        val parameter = JSONObject(postParamsMap)
        this.multipartBody.addPart(
            RequestBody.create(MediaType.parse("application/json; charset=utf-8"), parameter.toString())
        )
    }

    fun addFile(name: String, filePath: String, fileName: String) {
        this.multipartBody.addFormDataPart(
            name,
            fileName,
            RequestBody.create(MediaType.parse("image/png"), File(filePath))
        )
    }

    fun addZipFile(name: String, filePath: String, fileName: String) {
        this.multipartBody.addFormDataPart(
            name,
            fileName,
            RequestBody.create(MediaType.parse("application/zip"), File(filePath))
        )
    }

    fun sendHttpRequestUseOkHttp(url: String, method: String, headerParamsMap: Map<String, String>?, callback: Callback){
        val requestBody: RequestBody = multipartBody.build()
        val builder = Request.Builder()
        builder.url(url)

        if (headerParamsMap != null) {
            val headers = Headers.of(headerParamsMap.toMutableMap())
            builder.headers(headers)
        }
        if (HTTP_REQUEST_METHOD_POST == method) {
            builder.post(requestBody)
        }

        val request = builder.build()
        val call = okHttpClient.newCall(request)
        call.enqueue(callback)

    }
}