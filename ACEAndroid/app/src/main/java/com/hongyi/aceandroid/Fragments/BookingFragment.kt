package com.hongyi.aceandroid.Fragments

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import com.hongyi.aceandroid.R
import java.util.*
import android.widget.RadioGroup
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.afternoonTime
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.currentUser
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.eveningTime
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.morningTime
import com.hongyi.aceandroid.Activities.SignInActivity
import com.hongyi.aceandroid.Adapters.BookingTimeAdapter
import com.hongyi.aceandroid.Utils.*
import com.hongyi.aceandroid.Views.ToggleableRadioButton
import kotlinx.android.synthetic.main.fragment_booking.*
import kotlinx.android.synthetic.main.fragment_booking.view.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.text.SimpleDateFormat
import kotlin.collections.ArrayList

/**
 * Created by Hongyi on 2019-01-03.
 * @author Hongyi
 */

class BookingFragment : Fragment(), View.OnClickListener {
//    val checkedList = ArrayList<String>()
    private var morning = ArrayList<Int>()
    private var afternoon = ArrayList<Int>()
    private var evening = ArrayList<Int>()
    private lateinit var morningAdapter: BookingTimeAdapter
    private lateinit var afternoonAdapter: BookingTimeAdapter
    private lateinit var eveningAdapter: BookingTimeAdapter
    lateinit var handler: Handler
    lateinit var root: View

    companion object {
        fun newInstance(): BookingFragment {
            return BookingFragment()
        }
    }

    private val availabilityReceiver = object : BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            initTimeTable(root)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        morningTime.clear()
//        afternoonTime.clear()
//        eveningTime.clear()
//        testTime.forEach {
//            when (TimeUtil.getTimePeriod(it)) {
//                TimeUtil.MORNING -> morningTime.add(it)
//                TimeUtil.AFTERNOON -> afternoonTime.add(it)
//                TimeUtil.EVENING -> eveningTime.add(it)
//            }
//        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_booking, container, false)
        root = rootView
        val calendar = Calendar.getInstance()
        rootView.calendar_view.minDate = calendar.timeInMillis
//        calendar.add(Calendar.DAY_OF_MONTH, 30)
//        rootView.calendar_view.maxDate = calendar.getTimeInMillis()

        rootView.confirm_btn.setOnClickListener(this)

        addGuestRadioButton(rootView.guest_radio_group, 8)

        initTimeTable(root)

//        addCheckBox(rootView.morning_available, morningTime)
//        addCheckBox(rootView.afternoon_available, afternoonTime)
//        addCheckBox(rootView.evening_available, eveningTime)
//        addTimeRadioButton(rootView.morning_available, 8)
//        addTimeRadioButton(rootView.afternoon_available, 4)
//        addTimeRadioButton(rootView.evening_available, 6)
//        rootView.morning_available.setOnCheckedChangeListener { radioGroup, i ->
//            val radioMorning = rootView.morning_available.findViewById<ToggleableRadioButton>(i)
//            if (radioMorning != null && radioMorning.isChecked) {
//                val afternoonId = rootView.afternoon_available.checkedRadioButtonId
//                if (afternoonId != -1) {
//                    val radioAfternoon = rootView.afternoon_available.findViewById<ToggleableRadioButton>(afternoonId)
//                    radioAfternoon.toggle()
//                }
//                val eveningId = rootView.evening_available.checkedRadioButtonId
//                if (eveningId != -1) {
//                    val radioEvening = rootView.evening_available.findViewById<ToggleableRadioButton>(eveningId)
//                    radioEvening.toggle()
//                }
//            }
//        }
//        rootView.afternoon_available.setOnCheckedChangeListener { radioGroup, i ->
//            val radioAfternoon = rootView.afternoon_available.findViewById<ToggleableRadioButton>(i)
//            if (radioAfternoon != null && radioAfternoon.isChecked) {
//                val morningId = rootView.morning_available.checkedRadioButtonId
//                if (morningId != -1) {
//                    val radioMorning = rootView.morning_available.findViewById<ToggleableRadioButton>(morningId)
//                    radioMorning.toggle()
//                }
//                val eveningId = rootView.evening_available.checkedRadioButtonId
//                if (eveningId != -1) {
//                    val radioEvening = rootView.evening_available.findViewById<ToggleableRadioButton>(eveningId)
//                    radioEvening.toggle()
//                }
//            }
//        }
//        rootView.evening_available.setOnCheckedChangeListener { radioGroup, i ->
//            val radioEvening = rootView.evening_available.findViewById<ToggleableRadioButton>(i)
//            if (radioEvening != null && radioEvening.isChecked) {
//                val morningId = rootView.morning_available.checkedRadioButtonId
//                if (morningId != -1) {
//                    val radioMorning = rootView.morning_available.findViewById<ToggleableRadioButton>(morningId)
//                    radioMorning.toggle()
//                }
//                val afternoonId = rootView.afternoon_available.checkedRadioButtonId
//                if (afternoonId != -1) {
//                    val radioAfternoon = rootView.afternoon_available.findViewById<ToggleableRadioButton>(afternoonId)
//                    radioAfternoon.toggle()
//                }
//            }
//        }

        return rootView
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(activity as Context).registerReceiver(availabilityReceiver, IntentFilter("TODAY_AVAILABILITY"))
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(activity as Context).unregisterReceiver(availabilityReceiver)
    }

    override fun onClick(p0: View?) {
        when(p0){
            confirm_btn -> {
                Log.e(
                    "1234567890",
                    morningAdapter.checkedList.toString() + " " + afternoonAdapter.checkedList.toString() + "  " + eveningAdapter.checkedList.toString()
                )
                if (currentUser != null) {
                    if (morningAdapter.checkedList.isEmpty() && afternoonAdapter.checkedList.isEmpty() && eveningAdapter.checkedList.isEmpty()) {
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, "Please choose at least one interval to book", Snackbar.LENGTH_LONG)
                            .setAction("OK") {

                            }.addCallback(object : Snackbar.Callback() {
                                override fun onShown(sb: Snackbar?) {
                                    super.onShown(sb)
                                }

                                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                                    super.onDismissed(transientBottomBar, event)

                                }
                            }).show()
                    } else {
                        val builder = AlertDialog.Builder(activity as Context)
                        builder.setTitle("Booking")
                        builder.setMessage("Are you sure?")
                        builder.setCancelable(true)
                        builder.setPositiveButton("YES") { dialog, which ->
                            book()
                        }
                        builder.setNegativeButton("No") { dialog, which -> }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    }
                } else {
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, "Please login before you purchase", Snackbar.LENGTH_LONG)
                        .setAction("OK") {
                            val intent = Intent(context as Activity, SignInActivity::class.java)
                            startActivity(intent)
                        }.addCallback(object : Snackbar.Callback() {
                            override fun onShown(sb: Snackbar?) {
                                super.onShown(sb)
                            }

                            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                                super.onDismissed(transientBottomBar, event)

                            }
                        }).show()
                }
            }
        }
    }

    private fun addGuestRadioButton(radioGroup: RadioGroup, num : Int){
//        val density = resources.displayMetrics.density
        val radioButtonParams = RadioGroup.LayoutParams(200, ViewGroup.LayoutParams.WRAP_CONTENT)
//        val margin = (density * 6).toInt()
//        radioButtonParams.setMargins(10, 50, 10, 50)
        val inflater = activity!!.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        for (i in 1..num) {
            val radioButtonView = inflater.inflate(R.layout.radio_button_round, guest_radio_group, false)
            val radioButton = radioButtonView.findViewById<ToggleableRadioButton>(R.id.guest_btn)
            radioButton.id = i
            radioButton.text = i.toString()
            radioGroup.addView(radioButton, radioButtonParams)
        }
    }

//    fun addTimeRadioButton(radioGroup: RadioGroup, num: Int){
//        val radioButtonParams = RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 120)
//        radioButtonParams.setMargins(10, 10, 10, 10)
//        val inflater = activity!!.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        for (i in 1..num) {
//            val radioButtonView = inflater.inflate(R.layout.radio_button_square, radioGroup, false)
//            val radioButton = radioButtonView.findViewById<ToggleableRadioButton>(R.id.time_btn)
//            radioButton.id = i
//            radioButton.text = i.toString()
//            radioGroup.addView(radioButton, radioButtonParams)
//        }
//    }

//    fun addCheckBox(linearLayout: LinearLayout, timeList: ArrayList<Int>){
//        val checkBoxParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 120)
//        checkBoxParams.setMargins(10, 10, 10, 10)
//        val inflater = activity!!.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        timeList.forEach {
//            val checkBoxView = inflater.inflate(R.layout.check_box, linearLayout, false)
//            val checkBox = checkBoxView.findViewById<CheckBox>(R.id.time_btn)
//            checkBox.id = it
//            checkBox.text = hourMap[it]
//            linearLayout.addView(checkBox, checkBoxParams)
//            checkBox.setOnCheckedChangeListener { compoundButton, b ->
//                if (b) {
//                    checkedList.add(checkBox.text.toString())
//                } else {
//                    checkedList.remove(checkBox.text.toString())
//                }
//            }
//        }
//    }

//    fun getTime(){
//        val morningId = morning_available.checkedRadioButtonId
//        val afternoonId = afternoon_available.checkedRadioButtonId
//        val eveningId = evening_available.checkedRadioButtonId
//        var result = "nothing"
//        when {
//            morningId != -1 -> result = "morning $morningId"
//            afternoonId != -1 -> result = "afternoon $afternoonId"
//            eveningId != -1 -> result = "evening $eveningId"
//        }
//        Log.e("1234567890", result)
//    }

    fun getSelectedTime(time: Int){
        Log.e("1234567890", "get selected time is called $time")
        val milliTime = System.currentTimeMillis()
        calendar_view.setDate(milliTime, true, true)
        initTimeTable(root)
        when (TimeUtil.getTimePeriod(time)) {
            TimeUtil.MORNING -> {
//                val checkBox: CheckBox = morning_available.findViewById(time)
//                checkBox.isChecked = true
                morningAdapter.checkedList.add(time.toString())
                morningAdapter.notifyDataSetChanged()
            }
            TimeUtil.AFTERNOON -> {
//                val checkBox: CheckBox = afternoon_available.findViewById(time)
//                checkBox.isChecked = true
                afternoonAdapter.checkedList.add(time.toString())
                afternoonAdapter.notifyDataSetChanged()
            }
            TimeUtil.EVENING -> {
//                val checkBox: CheckBox = evening_available.findViewById(time)
//                checkBox.isChecked = true
                eveningAdapter.checkedList.add(time.toString())
                eveningAdapter.notifyDataSetChanged()
            }
        }
    }

    fun initTimeTable(rootView: View) {
        val morningLinearLayoutManager = LinearLayoutManager(activity)
        val afternoonLinearLayoutManager = LinearLayoutManager(activity)
        val eveningLinearLayoutManager = LinearLayoutManager(activity)
        rootView.morning_available.layoutManager = morningLinearLayoutManager
        rootView.afternoon_available.layoutManager = afternoonLinearLayoutManager
        rootView.evening_available.layoutManager = eveningLinearLayoutManager
        morning.clear()
        afternoon.clear()
        evening.clear()
        morning.addAll(morningTime)
        afternoon.addAll(afternoonTime)
        evening.addAll(eveningTime)
        morningAdapter = BookingTimeAdapter(activity as Context)
        afternoonAdapter = BookingTimeAdapter(activity as Context)
        eveningAdapter = BookingTimeAdapter(activity as Context)
        morningAdapter.setTimeList(morning)
        afternoonAdapter.setTimeList(afternoon)
        eveningAdapter.setTimeList(evening)
        rootView.morning_available.adapter = morningAdapter
        rootView.afternoon_available.adapter = afternoonAdapter
        rootView.evening_available.adapter = eveningAdapter
        rootView.calendar_view.setOnDateChangeListener { calendarView, year, month, day ->
            morningAdapter.checkedList.clear()
            afternoonAdapter.checkedList.clear()
            eveningAdapter.checkedList.clear()
            val date = year.toString() + "-" + (month+1).toString() + "-" + day.toString()
            getAvailability(date)
        }
    }

    fun getAvailability(date: String){
        handler = Handler(Handler.Callback { message ->
            val result = message.obj as ArrayList<*>
            if (message != null) {
                morning.clear()
                afternoon.clear()
                evening.clear()
                result.forEach {
                    when (TimeUtil.getTimePeriod(it as Int)) {
                        TimeUtil.MORNING -> morning.add(it)
                        TimeUtil.AFTERNOON -> afternoon.add(it)
                        TimeUtil.EVENING -> evening.add(it)
                    }
                }
            }
            morningAdapter.setTimeList(morning)
            afternoonAdapter.setTimeList(afternoon)
            eveningAdapter.setTimeList(evening)
            morningAdapter.notifyDataSetChanged()
            afternoonAdapter.notifyDataSetChanged()
            eveningAdapter.notifyDataSetChanged()
            true
        })
        val header = mapOf("Content-Type" to "application/jason")
        val body = JSONObject()
        body.put("bookingDate", date)
        OkHttpUtil.sendHttpRequestUseOkHttp(
            ACE_URL + AVAILABILITY_PREFIX_URL, "POST", header, body,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getJSONArray("data")
                        if (data != null) {
                            val time = ArrayList<Int>()
                            for (i in 0 until data.length()) {
                                val interval = data.getInt(i)
                                time.add(interval)
                            }
                            val message = Message()
                            message.obj = time
                            handler.sendMessage(message)
                        }
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }

    private fun book(){
        val header = mapOf("Content-Type" to "application/jason", "auth-token" to currentUser!!.token)
        val body = JSONObject()
        val intervals = JSONArray()
        val booked = ArrayList<String>()
        booked.addAll(morningAdapter.checkedList)
        booked.addAll(afternoonAdapter.checkedList)
        booked.addAll(eveningAdapter.checkedList)
        booked.forEach {
            intervals.put(it.toInt())
        }
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = format.format(Date(calendar_view.date))
        val selectedId = guest_radio_group.checkedRadioButtonId
        var guestNumber = 0
        if (selectedId != -1) {
            guestNumber = guest_radio_group.findViewById<RadioButton>(selectedId).text.toString().toInt()
        }
        body.put("intervalIDs", intervals)
        body.put("bookingDate", date)
        body.put("guestNumber", guestNumber)
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + PURCHASE_BOOKING_PREFIX_URL, "POST", header, body,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
//                        val data = jsonObjectResponse.getJSONObject("data")
                        activity?.runOnUiThread {
                            getAvailability(date)
                        }

                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }

}