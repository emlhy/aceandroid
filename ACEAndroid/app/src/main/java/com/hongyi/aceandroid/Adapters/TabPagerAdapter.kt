package com.hongyi.aceandroid.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

/**
 * Created by Hongyi on 2019-01-29.
 * @author Hongyi
 */

class TabPagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {
    private val TAB_MY_BOOKING = 0
    private val TAB_WALLET = 1
    private var fragmentList = ArrayList<Fragment>()

    override fun getItem(p0: Int): Fragment {
        return fragmentList[p0]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            TAB_MY_BOOKING -> return "My Booking"
            TAB_WALLET -> return "Wallet"
        }
        return null
    }

    fun addFragment(fragment: Fragment){
        fragmentList.add(fragment)
    }

}