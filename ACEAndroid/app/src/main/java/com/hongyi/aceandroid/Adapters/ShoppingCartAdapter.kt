package com.hongyi.aceandroid.Adapters

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.hongyi.aceandroid.Adapters.StoreAdapter.Companion.selectedCommodities
import com.hongyi.aceandroid.Entities.Commodity
import com.hongyi.aceandroid.R
import com.squareup.picasso.Picasso
import java.lang.ref.WeakReference
import java.net.URL

/**
 * Created by Hongyi on 2019-01-25.
 * @author Hongyi
 */

class ShoppingCartAdapter(private val context: Context, private val button: Button) : RecyclerView.Adapter<ShoppingCartAdapter.ViewHolder>() {
//    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    companion object {
        val cartList = ArrayList<Commodity>()
        var amountArray = HashMap<String, Int>()
        var subtotalArray = HashMap<String, Double>()
    }
    var focusPosition = -1
    lateinit var subtotalText: TextView
    lateinit var amountText: EditText
    var isRemoving = false
    private val textWatcher = object: TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            if (isRemoving) {
                notifyDataSetChanged()
                isRemoving = false
            } else {
                amountArray[cartList[focusPosition].uuid] = p0.toString().toInt()
                if (p0.toString() == "0") {
                    subtotalArray[cartList[focusPosition].uuid] = 0.0
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, "Do you want to remove this item from your shopping cart?", Snackbar.LENGTH_LONG)
                        .setAction(
                            "Yes"
                        ) {
                            isRemoving = true
                            amountArray.remove(cartList[focusPosition].uuid)
                            subtotalArray.remove(cartList[focusPosition].uuid)
                            selectedCommodities.removeAt(focusPosition)
                            cartList.removeAt(focusPosition)
                        }.addCallback(object : Snackbar.Callback() {

                            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                                super.onDismissed(transientBottomBar, event)
                                amountText.setText("1")
                            }
                        }).show()
                }
                if (p0.toString().isEmpty()) {
                    subtotalText.text = "$0"
                    subtotalArray[cartList[focusPosition].uuid] = 0.0
                } else {
                    val amount = amountArray[cartList[focusPosition].uuid]
                    val sub = cartList[focusPosition].price * if(amount == null){1}else{amount}
                    subtotalText.text = "$$sub"
                    subtotalArray[cartList[focusPosition].uuid] = sub
                }
                if (subtotalArray.size != 0) {
                    var total = 0.0
                    for ((key, value) in subtotalArray) {
                        total += subtotalArray[key]!!
                    }
                    button.text = "Total $$total"
                } else {
                    button.text = "Total"
                }
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ShoppingCartAdapter.ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_commodity_review, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cartList.size
    }

    override fun onBindViewHolder(holder: ShoppingCartAdapter.ViewHolder, position: Int) {
//        LoadImage(context, holder, cartList[position]).execute()
        Picasso.get().load(cartList[position].imageURL).into(holder.image)

//        holder.image.setImageDrawable(context.getDrawable(commodityList.get(cartList[position])))
//        holder.image.background = context.getDrawable(cartBackgroundList[position])
        holder.name.text = cartList[position].name
        holder.editAmount.setText(amountArray[cartList[position].uuid].toString())
        val amount = amountArray[cartList[position].uuid]
        holder.subTotalPrice.text = "$"+ (cartList[position].price * if(amount == null){1}else{amount}).toString()
        holder.editAmount.onFocusChangeListener = View.OnFocusChangeListener { p0, p1 ->
            if (p1) {
                focusPosition = position
                subtotalText = holder.subTotalPrice
                amountText = holder.editAmount
            }
        }
        holder.increaseButton.setOnClickListener {
            focusPosition = position
            subtotalText = holder.subTotalPrice
            amountText = holder.editAmount
            val currentAmount = holder.editAmount.text .toString()
            if (!currentAmount.isEmpty()) {
                if (currentAmount.toInt() < 99) {
                    holder.editAmount.setText((currentAmount.toInt() + 1).toString())
                    if (holder.editAmount.hasFocus()) {
                        holder.editAmount.setSelection(holder.editAmount.text.length)
                    }
//                    editTextArray.put(position, holder.editAmount.text.toString())
                }
            } else {
                holder.editAmount.setText("1")
                if (holder.editAmount.hasFocus()) {
                    holder.editAmount.setSelection(1)
                }
//                editTextArray.put(position, holder.editAmount.text.toString())
            }
        }
        holder.decreaseButton.setOnClickListener {
            focusPosition = position
            subtotalText = holder.subTotalPrice
            amountText = holder.editAmount
            val currentAmount = holder.editAmount.text .toString()
            if (!currentAmount.isEmpty()) {
                if (currentAmount.toInt() > 0) {
                    holder.editAmount.setText((currentAmount.toInt() - 1).toString())
                    if (holder.editAmount.hasFocus()) {
                        holder.editAmount.setSelection(holder.editAmount.text.length)
                    }
//                    editTextArray.put(position, holder.editAmount.text.toString())
                }
            } else {
                holder.editAmount.setText("0")
                if (holder.editAmount.hasFocus()) {
                    holder.editAmount.setSelection(1)
                }
//                editTextArray.put(position, holder.editAmount.text.toString())
            }
        }
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.editAmount.removeTextChangedListener(textWatcher)
        holder.editAmount.clearFocus()
//        if (focusPosition == holder.adapterPosition) {
//            inputMethodManager.hideSoftInputFromWindow(holder.editAmount.windowToken, 0)
//        }
    }

    override fun onViewAttachedToWindow(holder: ViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.editAmount.addTextChangedListener(textWatcher)
        if (focusPosition == holder.adapterPosition) {
            holder.editAmount.requestFocus()
            holder.editAmount.setSelection(holder.editAmount.text.length)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView = itemView.findViewById(R.id.item_image)
        var name: TextView = itemView.findViewById(R.id.item_name)
        var increaseButton: Button = itemView.findViewById(R.id.increase_btn)
        var decreaseButton: Button = itemView.findViewById(R.id.decrease_btn)
        var editAmount: EditText = itemView.findViewById(R.id.edit_amount)
        var subTotalPrice: TextView = itemView.findViewById(R.id.subtotal_price)
    }

    class LoadImage(val context: Context, val viewHolder: ShoppingCartAdapter.ViewHolder, val commodity: Commodity) :  AsyncTask<Void, Void, Void>() {
        private var contextRef: WeakReference<Context> = WeakReference(context)
        private var bitmap: Bitmap? = null
        override fun doInBackground(vararg params: Void): Void? {
            try {
                val inputStream = URL(commodity.imageURL).openStream()
                bitmap = BitmapFactory.decodeStream(inputStream)
            } catch (e: Exception) {

            }

            return null
        }

        override fun onPostExecute(result: Void) {
            if (bitmap != null) {
                viewHolder.image.setImageBitmap(bitmap)
            } else {
                //Bitmap.Config conf = Bitmap.Config.ARGB_8888;
                //Bitmap bmp = Bitmap.createBitmap(500, 500, conf);
                val icon = BitmapFactory.decodeResource(contextRef.get()?.resources, R.mipmap.ic_launcher)
                viewHolder.image.setImageBitmap(icon)
            }
        }
    }

    fun setCartList(cartList: ArrayList<Commodity>){
        Companion.cartList.clear()
        Companion.cartList.addAll(cartList)
        for (i in 0 until cartList.size){
            if (amountArray[cartList[i].uuid] == null)
                amountArray[cartList[i].uuid] = 1
            if (subtotalArray[cartList[i].uuid] == null)
                subtotalArray[cartList[i].uuid] = cartList[i].price
        }
    }
}