package com.hongyi.aceandroid.Entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Hongyi on 2019-01-28.
 * @author Hongyi
 */

@Entity(tableName = "user_table")
data class User(@PrimaryKey var uuid: String, @ColumnInfo(name = "nick_name") var nickName: String, @ColumnInfo(name = "phone_number") var phoneNumber: String, /*var gender: String,
                var birthday: String, var postcode: String,*/ var email: String?, @ColumnInfo(name = "qr_code") var qrCode: String,
                var role: Int, var profileLink: String?, var token: String) {
    constructor() : this("", "", "", /*"", "", "",*/ null, "", -1, null, "")

}