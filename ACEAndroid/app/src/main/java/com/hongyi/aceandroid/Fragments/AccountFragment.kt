package com.hongyi.aceandroid.Fragments

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hongyi.aceandroid.Activities.EditProfileActivity
import com.hongyi.aceandroid.Adapters.TabPagerAdapter
import com.hongyi.aceandroid.R
import kotlinx.android.synthetic.main.fragment_profile.view.*
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.currentUser
import com.hongyi.aceandroid.Activities.SignInActivity
import com.hongyi.aceandroid.Databases.Helpers.UserViewModel
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import java.lang.Exception

/**
 * Created by Hongyi on 2019-01-03.
 * @author Hongyi
 */

class AccountFragment : Fragment() {
    companion object {
        var index = -1
        fun newInstance(): AccountFragment {
            return AccountFragment()
        }
        fun getExpandedIndex(index: Int){
            this.index = index
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        Log.e("1234567890", "AccountFragment: onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_profile, container, false)
//        Log.e("1234567890", "AccountFragment: onCreateView")
        val tabPagerAdapter = TabPagerAdapter(childFragmentManager)
        tabPagerAdapter.addFragment(MyBookingFragment.newInstance())
        tabPagerAdapter.addFragment(WalletFragment.newInstance())
        rootView.profile_view_pager.adapter = tabPagerAdapter
        rootView.profile_tab_layout.setupWithViewPager(rootView.profile_view_pager)
//        rootView.log_out.setOnClickListener {
//            val userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
//            val builder = AlertDialog.Builder(activity as Context)
//            builder.setTitle("Log out")
//            builder.setMessage("Are you sure?")
//            builder.setCancelable(true)
//            builder.setPositiveButton("YES"){dialog, which ->
//                userViewModel.deleteAllUsers()
//                val intent = Intent(activity, SignInActivity::class.java)
//                startActivity(intent)
//                activity!!.finish()
//            }
//            builder.setNegativeButton("No"){dialog, which ->}
//            val dialog: AlertDialog = builder.create()
//            dialog.show()
//        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (currentUser != null){
            if (currentUser!!.profileLink != "null")
                Picasso.get().load(currentUser!!.profileLink).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(
                    NetworkPolicy.NO_CACHE).into(user_avatar, object: Callback{
                    override fun onSuccess() {

                    }

                    override fun onError(e: Exception?) {
                        user_avatar.setImageResource(R.mipmap.ic_launcher_round)
                    }

                })
            user_name.text = currentUser!!.nickName
            user_name.visibility = View.VISIBLE
//            log_out.visibility = View.VISIBLE
            user_avatar.setOnClickListener{
                val intent = Intent(activity, EditProfileActivity::class.java)
                startActivity(intent)
            }
        } else {
            user_name.visibility = View.GONE
//            log_out.visibility = View.GONE
        }
    }

    fun switchToWalletFragment() {
        if (index != -1) {
            app_bar_layout.setExpanded(false)
            profile_view_pager.post{
                profile_view_pager?.currentItem = 1
            }
        }
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        Log.e("1234567890", "AccountFragment: onViewCreate")
//    }
//
//    override fun onResume() {
//        super.onResume()
//        Log.e("1234567890", "AccountFragment: onResume")
//    }
//
//    override fun onPause() {
//        super.onPause()
//        Log.e("1234567890", "AccountFragment: onPause")
//    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//        Log.e("1234567890", "AccountFragment: onDestroyView")
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        Log.e("1234567890", "AccountFragment: onDestroy")
//    }
}