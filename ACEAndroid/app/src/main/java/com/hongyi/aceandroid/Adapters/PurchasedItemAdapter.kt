package com.hongyi.aceandroid.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hongyi.aceandroid.Entities.PurchasedItem
import com.hongyi.aceandroid.R

/**
 * Created by Hongyi on 2019-04-22.
 * @author Hongyi
 */

class PurchasedItemAdapter: RecyclerView.Adapter<PurchasedItemAdapter.ViewHolder>() {
    private val purchasedItems = ArrayList<PurchasedItem>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_purchased, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return purchasedItems.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.name.text = purchasedItems[p1].name
        p0.count.text = purchasedItems[p1].count.toString()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name = itemView.findViewById<TextView>(R.id.item_name)!!
        var count = itemView.findViewById<TextView>(R.id.item_count)!!
    }

    fun setItemList(bookingList: ArrayList<PurchasedItem>){
        this.purchasedItems.clear()
        this.purchasedItems.addAll(bookingList)
    }
}