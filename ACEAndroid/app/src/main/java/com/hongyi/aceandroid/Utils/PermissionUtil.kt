package com.hongyi.aceandroid.Utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog

/**
 * Created by Hongyi on 2019-01-14.
 * @author Hongyi
 */

object PermissionUtil {
    const val REQUEST_CODE_ASK_PERMISSIONS = 100

    @RequiresApi(api = Build.VERSION_CODES.M)
    fun permissionRequest(context: Context) {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String> ()
        if (!addPermission(context, permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write external storage")
        if (!addPermission(context, permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera")

        if (permissionsList.size > 0) {
            if (permissionsNeeded.size > 0) {
                var message = "You need to grant access to "
                for (i in 0 until permissionsNeeded.size)
                    message = message + ", " + permissionsNeeded[i]
                AlertDialog.Builder(context)
                    .setMessage(message)
                    .setPositiveButton("OK") { dialog, which -> ActivityCompat.requestPermissions(context as Activity, permissionsList.toArray(
                        arrayOfNulls<String>(permissionsList.size)), REQUEST_CODE_ASK_PERMISSIONS) }
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show()
                return
            }
            ActivityCompat.requestPermissions(context as Activity, permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)), REQUEST_CODE_ASK_PERMISSIONS)
            return
        }

    }

    private fun addPermission(context: Context, permissionsList : ArrayList<String>, permission : String): Boolean {
        if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission)

            if (!ActivityCompat.shouldShowRequestPermissionRationale(context as Activity, permission))
                return false
        }
        return true
    }
}