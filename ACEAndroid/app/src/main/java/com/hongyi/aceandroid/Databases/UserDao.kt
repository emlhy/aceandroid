package com.hongyi.aceandroid.Databases

import android.arch.persistence.room.*
import com.hongyi.aceandroid.Entities.User

/**
 * Created by Hongyi on 2019-02-04.
 * @author Hongyi
 */

@Dao
interface UserDao {
    @Insert
    fun insertUser(user: User)

    @Update
    fun updateUser(user: User)

    @Delete
    fun deleteUser(user: User)

    @Query("SELECT * FROM user_table WHERE phone_number = :phoneNumber")
    fun findUserByPhoneNumber(phoneNumber: String): User

    @Query("SELECT * FROM user_table LIMIT 1")
    fun getCurrentUser(): User

    @Query("DELETE FROM user_table")
    fun deleteAllUsers()
}