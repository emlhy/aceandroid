package com.hongyi.aceandroid.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.hourMap

/**
 * Created by Hongyi on 2019-01-31.
 * @author Hongyi
 */

class BookingTimeAdapter(private val context: Context): RecyclerView.Adapter<BookingTimeAdapter.ViewHolder>() {
    val checkedList = HashSet<String>()
    private val timeList = ArrayList<Int>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): BookingTimeAdapter.ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_booking_check_box, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return timeList.size
    }

    override fun onBindViewHolder(p0: BookingTimeAdapter.ViewHolder, p1: Int) {
        p0.timeCheckBox.text = hourMap[timeList[p1]]
        p0.timeCheckBox.id = timeList[p1]
        p0.timeCheckBox.isChecked = checkedList.contains(timeList[p1].toString())
//        checkedList.forEach {
//            if (p0.timeCheckBox.id == it.toInt())
//                p0.timeCheckBox.isChecked = true
//        }
        p0.timeCheckBox.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                checkedList.add(timeList[p1].toString())
            } else {
                checkedList.remove(timeList[p1].toString())
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var timeCheckBox: CheckBox = itemView.findViewById(R.id.time_btn)
    }

    fun setTimeList(timeList: ArrayList<Int>){
        this.timeList.clear()
        this.timeList.addAll(timeList)
    }
}