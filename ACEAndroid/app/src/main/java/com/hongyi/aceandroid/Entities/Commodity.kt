package com.hongyi.aceandroid.Entities

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Hongyi on 2019-02-11.
 * @author Hongyi
 */

data class Commodity(var uuid: String, var name: String, var price: Double, var typeId: Int, var imageURL: String) : Parcelable{
    constructor() : this("", "", 0.0, 0, "")
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(uuid)
        parcel.writeString(name)
        parcel.writeDouble(price)
        parcel.writeInt(typeId)
        parcel.writeString(imageURL)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Commodity> {
        override fun createFromParcel(parcel: Parcel): Commodity {
            val commodity = Commodity()
            commodity.uuid = parcel.readString()!!
            commodity.name = parcel.readString()!!
            commodity.price = parcel.readDouble()
            commodity.typeId = parcel.readInt()
            commodity.imageURL = parcel.readString()!!
//            return Commodity(parcel)
            return commodity
        }

        override fun newArray(size: Int): Array<Commodity?> {
            return arrayOfNulls(size)
        }
    }

}