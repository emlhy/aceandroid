package com.hongyi.aceandroid.Fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.currentUser
import com.hongyi.aceandroid.Adapters.BookingRecordAdapter
import com.hongyi.aceandroid.Entities.BookingRecord
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.*
import kotlinx.android.synthetic.main.fragment_my_booking.view.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

/**
 * Created by Hongyi on 2019-01-29.
 * @author Hongyi
 */

class MyBookingFragment : Fragment() {
    private var bookingRecords = ArrayList<BookingRecord>()
    lateinit var handler: Handler
    private lateinit var bookingRecordAdapter: BookingRecordAdapter
    private lateinit var recyclerView: RecyclerView

    companion object {
        fun newInstance(): MyBookingFragment {
            return MyBookingFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_my_booking, container, false)
        recyclerView = rootView.findViewById(R.id.booking_record)
        if (currentUser != null) {
            getBookingRecord()
        }
        return rootView
    }

    private fun initBookingRecord(){
        val bookingRecordLinearLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = bookingRecordLinearLayoutManager
        bookingRecordAdapter = BookingRecordAdapter(activity as Context, this)
        bookingRecordAdapter.setBookingList(bookingRecords)
        recyclerView.adapter = bookingRecordAdapter
    }

    fun getBookingRecord(){
        handler = Handler(Handler.Callback { message ->
            val result = message.obj as JSONArray
            if (message != null) {
                bookingRecords.clear()
                for(i in 0 until result.length()){
                    val record = result.getJSONObject(i)
                    val bookingRecord = BookingRecord(record.getString("uuid"), record.getString("bookingDate"),
                        hourMap.getValue(record.getInt("intervalID")), record.getInt("status"), record.getInt("guestNumber"), record.getInt("tableID"))
                    bookingRecords.add(bookingRecord)
                }
                initBookingRecord()
            }
            true
        })
        val header = mapOf("auth-token" to currentUser!!.token)
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + BOOKING_HISTORY_PREFIX_URL, "GET", header, null,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val view: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(view, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getJSONArray("data")
                        val message = Message()
                        message.obj = data
                        handler.sendMessage(message)
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val view: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }
}