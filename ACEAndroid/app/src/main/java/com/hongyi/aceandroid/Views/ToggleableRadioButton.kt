package com.hongyi.aceandroid.Views

import android.content.Context
import android.util.AttributeSet
import android.widget.RadioButton
import android.widget.RadioGroup
import com.hongyi.aceandroid.R

/**
 * Created by Hongyi on 2019-01-10.
 * @author Hongyi
 */

class ToggleableRadioButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.buttonStyle
) : RadioButton(context, attrs, defStyleAttr) {
    override fun toggle() {
        isChecked = !isChecked
        if(!isChecked){
//            background = ContextCompat.getDrawable(context, android.R.color.transparent)
//            setTextColor(ContextCompat.getColor(context, android.R.color.black))
            (parent as RadioGroup).clearCheck()
        } else {
//            for (i in 0 until (parent as RadioGroup).childCount) {
//                val radioButton = ((parent as RadioGroup).getChildAt(i)) as RadioButton
//                if (radioButton.isChecked){
//                    radioButton.background = ContextCompat.getDrawable(context, R.drawable.button_radio)
//                    radioButton.setTextColor(ContextCompat.getColor(context, android.R.color.white))
//                } else {
//                    radioButton.background = ContextCompat.getDrawable(context, android.R.color.transparent)
//                    radioButton.setTextColor(ContextCompat.getColor(context, android.R.color.black))
//                }
//                (parent as RadioGroup).getChildAt(i).background = ContextCompat.getDrawable(context, android.R.color.transparent)
//                (((parent as RadioGroup).getChildAt(i)) as RadioButton).setTextColor(ContextCompat.getColor(context, android.R.color.black))
//            }
//            background = ContextCompat.getDrawable(context, R.drawable.button_radio)
//            setTextColor(ContextCompat.getColor(context, android.R.color.white))
        }
    }
}