package com.hongyi.aceandroid.Utils

/**
 * Created by Hongyi on 2019-01-31.
 * @author Hongyi
 */

val hourMap = mapOf(
    0 to "0:00-1:00",
    1 to "1:00-2:00",
    2 to "2:00-3:00",
    3 to "3:00-4:00",
    4 to "4:00-5:00",
    5 to "5:00-6:00",
    6 to "6:00-7:00",
    7 to "7:00-8:00",
    8 to "8:00-9:00",
    9 to "9:00-10:00",
    10 to "10:00-11:00",
    11 to "11:00-12:00",
    12 to "12:00-13:00",
    13 to "13:00-14:00",
    14 to "14:00-15:00",
    15 to "15:00-16:00",
    16 to "16:00-17:00",
    17 to "17:00-18:00",
    18 to "18:00-19:00",
    19 to "19:00-20:00",
    20 to "20:00-21:00",
    21 to "21:00-22:00",
    22 to "22:00-23:00",
    23 to "23:00-0:00"
)

val testTime = arrayListOf(8, 9, 10, 12, 14, 15, 17, 18, 19, 21, 23)

object TimeUtil{
    const val MORNING = 0
    const val AFTERNOON = 1
    const val EVENING = 2
    fun getTimePeriod(tag: Int): Int{
        if (tag < 12)
            return MORNING
        else if (tag < 18)
            return AFTERNOON
        else
            return EVENING
    }
}