package com.hongyi.aceandroid.Activities

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.hongyi.aceandroid.R
import kotlinx.android.synthetic.main.activity_sign_in.*
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import com.hongyi.aceandroid.Databases.Helpers.UserViewModel
import com.hongyi.aceandroid.Entities.User
import com.hongyi.aceandroid.Utils.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException


/**
 * Created by Hongyi on 2019-01-02.
 * @author Hongyi
 */

class SignInActivity : AppCompatActivity() {
    lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        PermissionUtil.permissionRequest(this)

        initListener()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PermissionUtil.REQUEST_CODE_ASK_PERMISSIONS) {
            val perms = HashMap<String, Int>()
            perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
            perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED

            for (i in 0 until permissions.size)
                perms[permissions[i]] = grantResults[i]

            if (perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                && perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED) {

            } else {

            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun initListener(){
        close_btn.setOnClickListener {
            openHomeActivity()
        }

        sign_up_btn.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
        
        val loginHeader = mapOf("Content-Type" to "application/jason")
        sign_in_btn.setOnClickListener {
            val phoneNumber = input_phone_number.text.toString()
            val password = input_password.text.toString()
            if (phoneNumber.isEmpty() || password.isEmpty()) {
                if (phoneNumber.isEmpty()){
                    txt_input_phone_number.error = "Phone number cannot be empty"
                    txt_input_phone_number.isErrorEnabled = true
                }
                if (password.isEmpty()){
                    txt_input_password.error = "Password cannot be empty"
                    txt_input_password.isErrorEnabled = true
                }
            } else if (!ValidationUtil.isValidPhoneNumber(phoneNumber)){
                    txt_input_phone_number.error = "This is not a valid phone number"
                    txt_input_phone_number.isErrorEnabled = true
            } else {
                val loginBody = JSONObject()
                loginBody.put("phoneNumber", phoneNumber)
                loginBody.put("password", password)
                OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + LOGIN_PREFIX_URL, "POST", loginHeader, loginBody,
                    object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            Log.e("1234567890", e.toString())
                            Snackbar.make(sign_in_root, e.message.toString(), Snackbar.LENGTH_LONG).show()
                        }

                        override fun onResponse(call: Call, response: Response) {
                            val jsonObjectResponse = JSONObject(response.body()?.string())
                            if (response.isSuccessful) {
                                val data = jsonObjectResponse.getJSONObject("data")
                                val userData = data.getJSONObject("user")
                                val token = data.getString("token")
                                val user = User(userData.getString("uuid"),
                                    userData.getString("nickName"),
                                    userData.getString("phoneNumber"),
//                                    userData.getString("gender"),
//                                    userData.getString("birthday"),
//                                    userData.getString("postcode"),
                                    userData.getString("email"),
                                    userData.getString("qrCode"),
                                    userData.getInt("role"),
                                    userData.getString("profileLink"),
                                    token)
                                userViewModel.insertUser(user)

                                Log.e("1234567890", data.toString())
                                openHomeActivity()
                                finish()
                            } else {
                                val msg = jsonObjectResponse.getString("msg")
                                if (msg.contains("user not found")) {
                                    runOnUiThread {
                                        txt_input_phone_number.error = "User not found"
                                        txt_input_phone_number.isErrorEnabled = true
                                    }
                                } else if (msg.contains("password wrong")) {
                                    runOnUiThread {
                                        txt_input_password.error = "Wrong password"
                                        txt_input_password.isErrorEnabled = true
                                    }
                                }
                                Log.e("1234567890", msg)
                            }
                        }
                    })
            }
        }

        input_password.setOnFocusChangeListener { view, b ->
            if (b){
                val phoneNumber = input_phone_number.text.toString()
                if (phoneNumber.isNotEmpty() && !ValidationUtil.isValidPhoneNumber(phoneNumber)){
                    txt_input_phone_number.error = "This is not a valid phone number"
                    txt_input_phone_number.isErrorEnabled = true
                }
            }
        }

        input_password.setOnEditorActionListener(){view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE || (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER)){
                sign_in_btn.performClick()
                return@setOnEditorActionListener true
            }
            false
        }

        input_phone_number.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                if (p3 == 1 || p3 == 0){
//                    txt_input_phone_number.isErrorEnabled = false
//                }
                val phoneNumber = input_phone_number.text.toString()
                if (phoneNumber.isEmpty() || ValidationUtil.isValidPhoneNumber(phoneNumber)){
                    txt_input_phone_number.isErrorEnabled = false
                } else {
                    txt_input_phone_number.error = "This is not a valid phone number"
                    txt_input_phone_number.isErrorEnabled = true
                }
            }

        })

        input_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p3 == 1 || p3 == 0){
                    txt_input_password.isErrorEnabled = false
                }
            }

        })
    }

    fun openHomeActivity(){
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }
}
