package com.hongyi.aceandroid.Utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by Hongyi on 2019-01-22.
 * @author Hongyi
 */

object ValidationUtil {

    fun isValidPhoneNumber(phoneNumber : String) : Boolean{
        return Pattern.matches("[1-9][0-9]{9}", phoneNumber)
    }

    fun isValidEmail(email : String) : Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isValidPostCode(postCode : String) : Boolean{
        return Pattern.matches("[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]", postCode)
    }

    fun isValidBirthday(birthday : String) : Boolean{
        return if (birthday.length != 10){
            false
        } else {
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            format.isLenient = false
            try {
                val date = format.parse(birthday)
                val today = format.parse(format.format(Date()))
                val diff = (today.time - date.time)/1000/60/60/24
                diff in 1..36500
            } catch (e: ParseException) {
                e.printStackTrace()
                false
            }
        }
    }

}