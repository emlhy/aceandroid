package com.hongyi.aceandroid.Databases

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.hongyi.aceandroid.Entities.User
import kotlinx.coroutines.CoroutineScope

/**
 * Created by Hongyi on 2019-02-04.
 * @author Hongyi
 */

@Database(entities = [User::class], version = 1)
abstract class ACEDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: ACEDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): ACEDatabase{
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext, ACEDatabase::class.java, "ACE_Database"
                ).addCallback(ACEDatabaseCallback(scope)).build()
                INSTANCE = instance
                return instance
            }
        }
    }

    private class ACEDatabaseCallback(private val scope: CoroutineScope): RoomDatabase.Callback(){
        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
//            INSTANCE?.let { database ->
//                scope.launch { }
//            }
        }

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
        }
    }

    private fun addDelay() {
        try {
            Thread.sleep(4000)
        } catch (ignored: InterruptedException) {
        }

    }
}