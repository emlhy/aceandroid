package com.hongyi.aceandroid.Databases.Helpers

import android.support.annotation.WorkerThread
import com.hongyi.aceandroid.Databases.UserDao
import com.hongyi.aceandroid.Entities.User

/**
 * Created by Hongyi on 2019-02-04.
 * @author Hongyi
 */

class ACERepository(private val userDao: UserDao){
//    val currentUser: LiveData<User> = userDao.getCurrentUser()
    @WorkerThread
    suspend fun getCurrentUser(): User{
        return userDao.getCurrentUser()
    }

    @WorkerThread
    suspend fun findUserByPhoneNumber(phoneNumber: String): User{
        return userDao.findUserByPhoneNumber(phoneNumber)
    }

    @WorkerThread
    suspend fun insertUser(user: User){
        userDao.insertUser(user)
    }

    @WorkerThread
    suspend fun updateUser(user: User){
        userDao.updateUser(user)
    }

    @WorkerThread
    suspend fun deleteUser(user: User){
        userDao.deleteUser(user)
    }

    @WorkerThread
    suspend fun deleteAllUsers(){
        userDao.deleteAllUsers()
    }
}