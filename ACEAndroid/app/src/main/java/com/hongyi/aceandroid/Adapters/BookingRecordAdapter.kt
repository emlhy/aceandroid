package com.hongyi.aceandroid.Adapters

import android.app.Activity
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.currentUser
import com.hongyi.aceandroid.Entities.BookingRecord
import com.hongyi.aceandroid.Fragments.MyBookingFragment
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.ACE_URL
import com.hongyi.aceandroid.Utils.CANCEL_PENDING_BOOKING_PREFIX_URL
import com.hongyi.aceandroid.Utils.OkHttpUtil
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException
import java.lang.ref.WeakReference

/**
 * Created by Hongyi on 2019-02-11.
 * @author Hongyi
 */

class BookingRecordAdapter(private val context: Context, private val fragmentContext: MyBookingFragment): RecyclerView.Adapter<BookingRecordAdapter.ViewHolder>(){
    private val bookingList = ArrayList<BookingRecord>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_up_comming_booking, p0, false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bookingList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bookingDate.text = bookingList[p1].bookingDate
        p0.bookingTime.text = bookingList[p1].bookingTime
        if (bookingList[p1].status == 0) {
            p0.cancelBtn.visibility = View.VISIBLE
            p0.cancelBtn.setOnClickListener {
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Cancel Pending Booking")
                builder.setMessage("Are you sure?")
                builder.setCancelable(true)
                builder.setPositiveButton("YES") { dialog, which ->
                    cancelPendingBooking(bookingList[p1].uuid)
                }
                builder.setNegativeButton("No") { dialog, which -> }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        } else {
            p0.cancelBtn.visibility = View.INVISIBLE
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var bookingDate = itemView.findViewById<TextView>(R.id.booking_date)!!
        var bookingTime = itemView.findViewById<TextView>(R.id.booking_time)!!
        var cancelBtn = itemView.findViewById<Button>(R.id.cancel_booking)!!
    }

    fun setBookingList(bookingList: ArrayList<BookingRecord>){
        this.bookingList.clear()
        this.bookingList.addAll(bookingList)
    }

    private fun cancelPendingBooking(uuid: String){
        val header = mapOf("Content-Type" to "application/jason", "auth-token" to currentUser!!.token)
        val body = JSONObject()
        body.put("bookingID", uuid)
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + CANCEL_PENDING_BOOKING_PREFIX_URL, "POST", header, body,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val fragmentReference = WeakReference<MyBookingFragment>(fragmentContext)
                        val myBookingFragment = fragmentReference.get()
                        (context as Activity).runOnUiThread {
                            myBookingFragment?.getBookingRecord()
                        }

                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }
}