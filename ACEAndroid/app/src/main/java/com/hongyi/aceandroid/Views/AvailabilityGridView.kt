package com.hongyi.aceandroid.Views

import android.content.Context
import android.util.AttributeSet
import android.widget.GridView

/**
 * Created by Hongyi on 2019-01-03.
 * @author Hongyi
 */

class AvailabilityGridView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : GridView(context, attrs, defStyleAttr) {
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val expandSpec : Int = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE shr 2, MeasureSpec.AT_MOST)
        super.onMeasure(widthMeasureSpec, expandSpec)
    }
}