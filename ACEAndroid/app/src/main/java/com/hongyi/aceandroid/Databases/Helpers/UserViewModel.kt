package com.hongyi.aceandroid.Databases.Helpers

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.hongyi.aceandroid.Databases.ACEDatabase
import com.hongyi.aceandroid.Entities.User
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import kotlin.coroutines.CoroutineContext

/**
 * Created by Hongyi on 2019-02-04.
 * @author Hongyi
 */

class UserViewModel(application: Application) : AndroidViewModel(application) {
    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository: ACERepository
//    val currentUser: LiveData<User>

    init {
        val userDao = ACEDatabase.getDatabase(application, scope).userDao()
        repository = ACERepository(userDao)
//        currentUser = repository.currentUser
    }

    fun getCurrentUser(): User? = runBlocking {
        var user: User? = null
        val job = scope.launch(Dispatchers.IO) {
            user = repository.getCurrentUser()
        }
        job.join()
        return@runBlocking user
    }

    fun findUserByPhoneNumber(phoneNumber: String): User? = runBlocking{
        var user: User? = null
        val job = scope.launch(Dispatchers.IO) {
            user = repository.findUserByPhoneNumber(phoneNumber)
        }
        job.join()
        return@runBlocking user
    }


    fun insertUser(user: User) = scope.launch(Dispatchers.IO){
        repository.insertUser(user)
    }

    fun updateUser(user: User) = scope.launch(Dispatchers.IO){
        repository.updateUser(user)
    }

    fun deleteUser(user: User) = scope.launch(Dispatchers.IO){
        repository.deleteUser(user)
    }

    fun deleteAllUsers() = scope.launch(Dispatchers.IO){
        repository.deleteAllUsers()
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }
}