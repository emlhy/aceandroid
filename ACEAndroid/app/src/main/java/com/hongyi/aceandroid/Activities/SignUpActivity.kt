package com.hongyi.aceandroid.Activities

import android.content.Intent
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.ImageView
import android.widget.RadioButton
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception

/**
 * Created by Hongyi on 2019-01-06.
 * @author Hongyi
 */

class SignUpActivity : AppCompatActivity(){
    private val REQUEST_CODE_IMAGE = 101
    private val folderPath = Environment.getExternalStorageDirectory().path + File.separator + "ACE"
    private val aceFolder = File(folderPath)
    private val tempFile = File(folderPath + File.separator + "avatar.jpg")
    private var image : Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        initListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        tempFile.delete()
}

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK){
            if (requestCode == REQUEST_CODE_IMAGE){
                lateinit var bitmap : Bitmap
                if (data?.data == null){
                    bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        val uri = FileProvider.getUriForFile(this, "com.hongyi.aceandroid.fileprovider", tempFile)
                        MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                    } else {
                        MediaStore.Images.Media.getBitmap(this.contentResolver, Uri.fromFile(tempFile))
                    }
                    bitmap = ImageUtil.rotateImage(tempFile.path, bitmap)
                } else {
                    val uri = data.data
                    bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                }
                bitmap = ThumbnailUtils.extractThumbnail(bitmap, 512, 512)
                try {
                    val out = FileOutputStream(tempFile)
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
                    out.flush()
                    out.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                image = bitmap
                choose_image.setPadding(0, 0, 0, 0)
                choose_image.scaleType = ImageView.ScaleType.CENTER
                choose_image.setImageBitmap(bitmap)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun initListener(){
        submit_btn.setOnClickListener {
            val username = edit_username.text.toString()
            val phoneNumber = edit_phone_number.text.toString()
            val password = edit_password.text.toString()
            val confirmPassword = edit_confirm_password.text.toString()
//            val gender = if(radio_gender.checkedRadioButtonId == -1) "" else (findViewById<RadioButton>(radio_gender.checkedRadioButtonId)).text.toString()
//            val birthday = edit_birthday.text.toString()
//            val postcode = edit_postcode.text.toString()
            val email = edit_email.text.toString()
            val emergencyContact = edit_emergency_contact.text.toString()
            val emergencyNumber = edit_emergency_number.text.toString()
            val image = this.image
            if (username.isEmpty() || phoneNumber.isEmpty() || password.isEmpty() || email.isEmpty()){ //|| gender.isEmpty() || birthday.isEmpty() || postcode.isEmpty()){
                if (username.isEmpty()){
                    txt_username.error = "Username cannot be empty"
                    txt_username.isErrorEnabled = true
                }
                if (phoneNumber.isEmpty()){
                    txt_phone_number.error = "Phone number cannot be empty"
                    txt_phone_number.isErrorEnabled = true
                }
                if (password.isEmpty()){
                    txt_password.error = "Password cannot be empty"
                    txt_password.isErrorEnabled = true
                }
                if (email.isEmpty()){
                    txt_email.error = "Email cannot be empty"
                    txt_email.isErrorEnabled = true
                }
//                if (gender.isEmpty()){
//                    txt_gender.error = "Please select your gender"
//                    txt_gender.isErrorEnabled = true
//                }
//                if (birthday.isEmpty()){
//                    txt_birthday.error = "Birthday cannot be empty"
//                    txt_birthday.isErrorEnabled = true
//                }
//                if (postcode.isEmpty()){
//                    txt_postcode.error = "Postcode cannot be empty"
//                    txt_postcode.isErrorEnabled = true
//                }
            } else if (!ValidationUtil.isValidPhoneNumber(phoneNumber)){
                txt_phone_number.error = "This is not a valid phone number"
                txt_phone_number.isErrorEnabled = true
//            } else if (!ValidationUtil.isValidPostCode(postcode.toUpperCase())){
//                txt_postcode.error = "This is not a valid postcode"
//                txt_postcode.isErrorEnabled = true
            } else if (password != confirmPassword){
                txt_confirm_password.error = "Must match the previous entry"
                txt_confirm_password.isErrorEnabled = true
//            } else if (!birthday.isEmpty() && !ValidationUtil.isValidBirthday(birthday)){
//                txt_birthday.error = "This is not a valid birthday"
//                txt_birthday.isErrorEnabled = true
            } else if (!ValidationUtil.isValidEmail(email)){
                txt_email.error = "This is not a valid email"
                txt_email.isErrorEnabled = true
            } else if (emergencyNumber.isNotEmpty() && !ValidationUtil.isValidPhoneNumber(emergencyNumber)) {
                txt_emergency_number.error = "This is not a valid phone number"
                txt_emergency_number.isErrorEnabled = true
            }else {
                if (image != null){
                    uploadAvatarAndSignUp(username, phoneNumber, password, email, emergencyContact, emergencyNumber, tempFile)
                } else
                    signUp(username, phoneNumber, password, email, emergencyContact, emergencyNumber, "")
            }
        }

        choose_image.setOnClickListener {
            if (!aceFolder.exists())
                aceFolder.mkdirs()
            val galleryIntent = Intent(Intent.ACTION_PICK, null)
            galleryIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*")
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (tempFile.exists()){
                tempFile.delete()
                tempFile.createNewFile()
            } else {
                tempFile.createNewFile()
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                cameraIntent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                val uri = FileProvider.getUriForFile(this, "com.hongyi.aceandroid.fileprovider", tempFile)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            } else {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile))
            }
            val chooser = Intent.createChooser(galleryIntent, "Please choose an app for your image")
            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(cameraIntent))
            startActivityForResult(chooser, REQUEST_CODE_IMAGE)
        }

        privacy_policy_ckb.setOnCheckedChangeListener { compoundButton, b ->
            if (b)
                enableSubmitButton()
            else
                disableSubmitButton()
        }

        privacy_policy.setOnClickListener {
            val privacyPolicyDialogBuilder = AlertDialog.Builder(this)
            val privacyPolicyDialogView = layoutInflater.inflate(R.layout.view_privacy_policy, null)
            privacyPolicyDialogBuilder.setCancelable(true)
            privacyPolicyDialogBuilder.setView(privacyPolicyDialogView)
            val privacyPolicyDialog = privacyPolicyDialogBuilder.create()
            privacyPolicyDialog.show()
            privacyPolicyDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
//            val noBtn = privacyPolicyDialogView.findViewById(R.id.no_btn)
//            noBtn.setOnClickListener(View.OnClickListener { mobileDataAlertDialog.dismiss() })
//            val yesBtn = privacyPolicyDialogView.findViewById(R.id.yes_btn)
//            yesBtn.setOnClickListener(View.OnClickListener { mobileDataAlertDialog.dismiss() })
        }

        edit_username.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p3 == 1 || p3 == 0){
                    txt_username.isErrorEnabled = false
                }
            }

        })

        edit_phone_number.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                if (p3 == 1 || p3 == 0){
//                    txt_phone_number.isErrorEnabled = false
//                }
                val phoneNumber = edit_phone_number.text.toString()
                if (phoneNumber.isEmpty() || ValidationUtil.isValidPhoneNumber(phoneNumber)){
                    txt_phone_number.isErrorEnabled = false
                } else {
                    txt_phone_number.error = "This is not a valid phone number"
                    txt_phone_number.isErrorEnabled = true
                }
            }

        })

        edit_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p3 == 1 || p3 == 0){
                    txt_password.isErrorEnabled = false
                }
            }

        })

        /*edit_postcode.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                if (p3 == 1 || p3 == 0){
//                    txt_postcode.isErrorEnabled = false
//                }
                val postcode = edit_postcode.text.toString()
                if (postcode.isEmpty() || ValidationUtil.isValidPostCode(postcode.toUpperCase())){
                    txt_postcode.isErrorEnabled = false
                } else {
                    txt_postcode.error = "This is not a valid postcode"
                    txt_postcode.isErrorEnabled = true
                }
            }

        })*/

//        radio_gender.setOnCheckedChangeListener { radioGroup, i ->
//            if (i != -1)
//                txt_gender.isErrorEnabled = false
//        }

        /*edit_birthday.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                if (p3 == 1 || p3 == 0){
//                    txt_birthday.isErrorEnabled = false
//                }
                val birthday = edit_birthday.text.toString()
                if (birthday.isEmpty() || ValidationUtil.isValidBirthday(birthday)){
                    txt_birthday.isErrorEnabled = false
                } else {
                    txt_birthday.error = "This is not a valid birthday"
                    txt_birthday.isErrorEnabled = true
                }
                if (p3 > 0 && (p1 == 3 || p1 == 6)){
                    edit_birthday.append("-")
                }
            }

        })*/

        edit_email.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                if (p3 == 1 || p3 == 0){
//                    txt_email.isErrorEnabled = false
//                }
                val email = edit_email.text.toString()
                if (email.isEmpty() || ValidationUtil.isValidEmail(email)){
                    txt_email.isErrorEnabled = false
                } else {
                    txt_email.error = "This is not a valid email"
                    txt_email.isErrorEnabled = true
                }
            }

        })

        edit_confirm_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                if (p3 == 1 || p3 == 0){
//                    txt_email.isErrorEnabled = false
//                }
                val password = edit_password.text.toString()
                val confirmPassword = edit_confirm_password.text.toString()
                if (password == confirmPassword){
                    txt_confirm_password.isErrorEnabled = false
                } else {
                    txt_confirm_password.error = "Must match the previous entry"
                    txt_confirm_password.isErrorEnabled = true
                }
            }

        })

        edit_emergency_number.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val phoneNumber = edit_emergency_number.text.toString()
                if (phoneNumber.isEmpty() || ValidationUtil.isValidPhoneNumber(phoneNumber)){
                    txt_phone_number.isErrorEnabled = false
                } else {
                    txt_phone_number.error = "This is not a valid phone number"
                    txt_phone_number.isErrorEnabled = true
                }
            }

        })
    }

    private fun signUp(username: String, phoneNumber: String, password: String, email: String, emergencyContact: String, emergencyNumber: String, profileLink: String){
        val registerHeader = mapOf("Content-Type" to "application/jason")
        val registerBody = JSONObject()
        registerBody.put("nickName", username)
        registerBody.put("phoneNumber", phoneNumber)
        registerBody.put("password", password)
//                registerBody.put("gender", gender)
//                registerBody.put("postcode", postcode)
//                if (!birthday.isEmpty())
//                    registerBody.put("birthday", birthday)
        if (email.isNotEmpty())
            registerBody.put("email", email)
        if (emergencyContact.isNotEmpty())
            registerBody.put("emergencyContact", emergencyContact)
        if (emergencyNumber.isNotEmpty())
            registerBody.put("emergencyNumber", emergencyNumber)
        if (profileLink.isNotEmpty())
            registerBody.put("profileLink", profileLink)
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + REGISTER_PREFIX_URL, "POST", registerHeader, registerBody, object : Callback{
            //                val okHttpTest = OkHttpTest()
//                okHttpTest.addFile("profileLink", tempFile.path, "tempFile")
//                okHttpTest.addString(registerBody)
//                okHttpTest.sendHttpRequestUseOkHttp(ACE_URL + REGISTER_PREFIX_URL, "POST", registerHeader, object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.e("1234567890", e.toString())
                Snackbar.make(sign_up_root, e.message.toString(), Snackbar.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call, response: Response) {
                val jsonObjectResponse = JSONObject(response.body()?.string())
                if (response.isSuccessful) {
                    val data = jsonObjectResponse.getString("data")
                    Log.e("1234567890", data)
                    finish()
                } else {
                    val msg = jsonObjectResponse.getString("msg")
                    if (msg.contains("already exist")) {
                        runOnUiThread {
                            txt_phone_number.error = "User already exist"
                            txt_phone_number.isErrorEnabled = true
                        }
                    }
                    Log.e("1234567890", msg)
                }
            }

        })
    }

    private fun uploadAvatarAndSignUp(username: String, phoneNumber: String, password: String, email: String, emergencyContact: String, emergencyNumber: String, tempFile: File){
        OkHttpUtil.uploadImage(ACE_URL + UPLOAD_AVATAR_PREFIX_URL, tempFile, null, object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                Snackbar.make(sign_up_root, e.message.toString(), Snackbar.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call, response: Response) {
                val jsonObjectResponse = JSONObject(response.body()?.string())
                if (response.isSuccessful) {
                    val profileLink = jsonObjectResponse.getString("imageUrl")
                    signUp(username, phoneNumber, password, email, emergencyContact, emergencyNumber, profileLink)
                } else {

                }
            }

        })
    }

    private fun disableSubmitButton(){
        submit_btn.alpha = .5f
        submit_btn.isEnabled = false
    }

    private fun enableSubmitButton(){
        submit_btn.alpha = 1f
        submit_btn.isEnabled = true
    }

}