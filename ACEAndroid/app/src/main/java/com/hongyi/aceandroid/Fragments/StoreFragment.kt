package com.hongyi.aceandroid.Fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hongyi.aceandroid.Adapters.StoreAdapter
import com.hongyi.aceandroid.Adapters.StoreAdapter.Companion.selectedCommodities
import com.hongyi.aceandroid.Entities.Commodity
import com.hongyi.aceandroid.Fragments.ShoppingCartFragment.Companion.checkoutCompleteForStore
//import com.hongyi.aceandroid.Fragments.RootFragment.FragmentState.isStoreFragment
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.ACE_URL
import com.hongyi.aceandroid.Utils.OkHttpUtil
import com.hongyi.aceandroid.Utils.STORE_ITEMS_PREFIX_URL
import kotlinx.android.synthetic.main.fragment_store.*
import kotlinx.android.synthetic.main.fragment_store.view.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

/**
 * Created by Hongyi on 2019-01-03.
 * @author Hongyi
 */

//val commodityList = arrayListOf<Int>(
////            R.drawable.ic_account,
////            R.drawable.ic_home,
////            R.drawable.ic_calendar,
////            R.drawable.ic_cart,
////            R.drawable.ic_ping_pong,
////            R.drawable.ic_account,
////            R.drawable.ic_home
//    R.drawable.temp_badminton,
//    R.drawable.temp_basketball,
//    R.drawable.temp_burrito,
//    R.drawable.temp_chocolate,
//    R.drawable.temp_fries,
//    R.drawable.temp_hot_dog,
//    R.drawable.temp_ice_cream,
//    R.drawable.temp_pizza,
//    R.drawable.temp_popcorn,
//    R.drawable.temp_salad,
//    R.drawable.temp_cake,
//    R.drawable.temp_table_tennis
//)
//
//val storeBackgroundList = arrayListOf<Int>(
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_white
//)
//
//val cartBackgroundList = arrayListOf<Int>(
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint,
//    R.drawable.radio_round_corner_white,
//    R.drawable.radio_round_corner_mint
//)

class StoreFragment : Fragment() {
    lateinit var handler: Handler
    private lateinit var storeAdapter: StoreAdapter
    private val commodityList = ArrayList<Commodity>()


    companion object {
        fun newInstance(): StoreFragment {
            return StoreFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_store, container, false)
//        isStoreFragment = true

        getCommodityList(rootView)

        rootView.shopping_cart_btn.setOnClickListener {
//            val intent = Intent(activity, ShoppingCartActivity::class.java)
//            startActivity(intent)
            if (!selectedCommodities.isEmpty()) {
                val fragmentTransaction = activity!!.supportFragmentManager!!.beginTransaction()
                val bundle = Bundle()
                bundle.putParcelableArrayList("selected_item", selectedCommodities)
                val shoppingCartFragment = ShoppingCartFragment()
                shoppingCartFragment.arguments = bundle
                fragmentTransaction.replace(R.id.root_frame, shoppingCartFragment)
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            } else {
                val activityView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                Snackbar.make(activityView, "There is no item in your shopping cart", Snackbar.LENGTH_LONG).show()
            }
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (checkoutCompleteForStore) {
            selectedCommodities.clear()
            storeAdapter.setCommodityList(commodityList)
            storeAdapter.notifyDataSetChanged()
            checkoutCompleteForStore = false
        }
        if (!selectedCommodities.isEmpty()){
            shopping_cart_btn.text = "Shopping Cart(" + selectedCommodities.size.toString() + ")"
        } else {
            shopping_cart_btn.text = "Shopping Cart"
        }
    }

    private fun initCommodityList(rootView: View){
        val gridLayoutManager = GridLayoutManager(activity, 2)
        rootView.commodity_list.layoutManager = gridLayoutManager
        storeAdapter = StoreAdapter(activity as Context, rootView.shopping_cart_btn)
        storeAdapter.setCommodityList(commodityList)
        rootView.commodity_list.adapter = storeAdapter
    }

    private fun getCommodityList(rootView: View){
        handler = Handler(Handler.Callback { message ->
            val result = message.obj as JSONArray
            if (message != null) {
                commodityList.clear()
                for(i in 0 until result.length()){
                    val record = result.getJSONObject(i)
                    val commodity = Commodity(record.getString("uuid"), record.getString("name"), record.getDouble("price"), record.getInt("typeId"), record.getString("imageURL"))
                    commodityList.add(commodity)
                }
                initCommodityList(rootView)
            }
            true
        })
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + STORE_ITEMS_PREFIX_URL, "GET", null, null,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val view: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(view, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getJSONArray("data")
                        val message = Message()
                        message.obj = data
                        handler.sendMessage(message)
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val view: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }
}