package com.hongyi.aceandroid.Activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.hongyi.aceandroid.Databases.Helpers.UserViewModel
import com.hongyi.aceandroid.R

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorAccent)
        window.navigationBarColor = ContextCompat.getColor(this, R.color.colorAccent)

        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            val userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
            if (userViewModel.getCurrentUser() != null){
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this, SignInActivity::class.java)
                startActivity(intent)
            }
            finish()
        }, 2000)
    }
}