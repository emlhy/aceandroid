package com.hongyi.aceandroid.Entities

/**
 * Created by Hongyi on 2019-02-11.
 * @author Hongyi
 */

data class BookingRecord(var uuid: String, var bookingDate: String, var bookingTime: String, var status: Int, var guestNumber: Int, var tableId: Int) {
}