package com.hongyi.aceandroid.Fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.afternoonTime
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.eveningTime
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.morningTime
import com.hongyi.aceandroid.Adapters.AutoScrollViewAdapter
import com.hongyi.aceandroid.Adapters.TimeAdapter
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlin.collections.ArrayList

/**
 * Created by Hongyi on 2019-01-03.
 * @author Hongyi
 */

class HomeFragment : Fragment() {
    private lateinit var onSwitchFragmentListener: OnSwitchFragmentListener
    private lateinit var morningAdapter: TimeAdapter
    private lateinit var afternoonAdapter: TimeAdapter
    private lateinit var eveningAdapter: TimeAdapter
    private var morning = ArrayList<Int>()
    private var afternoon = ArrayList<Int>()
    private var evening = ArrayList<Int>()
    lateinit var root: View

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    private val availabilityReceiver = object : BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            initTimeTable(root)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        morningTime.clear()
//        afternoonTime.clear()
//        eveningTime.clear()
//        testTime.forEach {
//            when (TimeUtil.getTimePeriod(it)) {
//                MORNING -> morningTime.add(it)
//                AFTERNOON -> afternoonTime.add(it)
//                EVENING -> eveningTime.add(it)
//            }
//        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        root = rootView
        val adapter = AutoScrollViewAdapter(activity as Context)
        rootView.gallery_view.setAdapter(adapter)
        rootView.gallery_view.setAutoPlay(true)
//        addTimeButton(rootView.morning_available, morningTime)
//        addTimeButton(rootView.afternoon_available, afternoonTime)
//        addTimeButton(rootView.evening_available, eveningTime)

        initTimeTable(rootView)

        rootView.membership_card.setOnClickListener {
            onSwitchFragmentListener.switchToProfile(0)
        }
        rootView.top_up_card.setOnClickListener {
            onSwitchFragmentListener.switchToProfile(1)
        }
        rootView.balance_card.setOnClickListener {
            onSwitchFragmentListener.switchToProfile(2)
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(activity as Context).registerReceiver(availabilityReceiver, IntentFilter("TODAY_AVAILABILITY"))
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(activity as Context).unregisterReceiver(availabilityReceiver)
    }

    private fun addTimeButton(linearLayout: LinearLayout, timeList: ArrayList<Int>){
        val buttonParameters = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 120)
        buttonParameters.setMargins(10, 10, 10, 10)
        timeList.forEach {
            val button = Button(activity)
            button.background = ContextCompat.getDrawable(activity as Context, R.drawable.button_round_corner_white)
            button.id = it
            button.text = hourMap[it]
            linearLayout.addView(button, buttonParameters)
            button.setOnClickListener {
                onSwitchFragmentListener.switchToBooking(button.id)
                Log.e("1234567890", linearLayout.id.toString() + " " + button.id.toString())
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onSwitchFragmentListener = activity as OnSwitchFragmentListener
    }

    interface OnSwitchFragmentListener {
        fun switchToProfile(index: Int)
        fun switchToBooking(time: Int)
    }

    fun initTimeTable(rootView: View){
        val morningLinearLayoutManager = LinearLayoutManager(activity)
        val afternoonLinearLayoutManager = LinearLayoutManager(activity)
        val eveningLinearLayoutManager = LinearLayoutManager(activity)
        rootView.morning_available.layoutManager = morningLinearLayoutManager
        rootView.afternoon_available.layoutManager = afternoonLinearLayoutManager
        rootView.evening_available.layoutManager = eveningLinearLayoutManager
        morning.clear()
        afternoon.clear()
        evening.clear()
        morning.addAll(morningTime)
        afternoon.addAll(afternoonTime)
        evening.addAll(eveningTime)
        morningAdapter = TimeAdapter(activity as Context, onSwitchFragmentListener)
        afternoonAdapter = TimeAdapter(activity as Context, onSwitchFragmentListener)
        eveningAdapter = TimeAdapter(activity as Context, onSwitchFragmentListener)
        morningAdapter.setTimeList(morning)
        afternoonAdapter.setTimeList(afternoon)
        eveningAdapter.setTimeList(evening)
        rootView.morning_available.adapter = morningAdapter
        rootView.afternoon_available.adapter = afternoonAdapter
        rootView.evening_available.adapter = eveningAdapter
    }
}