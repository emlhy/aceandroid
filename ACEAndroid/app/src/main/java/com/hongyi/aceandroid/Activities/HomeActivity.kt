package com.hongyi.aceandroid.Activities

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.hongyi.aceandroid.Adapters.ViewpagerAdapter
import com.hongyi.aceandroid.Databases.Helpers.UserViewModel
import com.hongyi.aceandroid.Entities.User
import com.hongyi.aceandroid.Fragments.*
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.ACE_URL
import com.hongyi.aceandroid.Utils.AVAILABILITY_PREFIX_URL
import com.hongyi.aceandroid.Utils.OkHttpUtil
import com.hongyi.aceandroid.Utils.TimeUtil
import kotlinx.android.synthetic.main.activity_home.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Hongyi on 2019-01-03.
 * @author Hongyi
 */

class HomeActivity : AppCompatActivity(), HomeFragment.OnSwitchFragmentListener {
    companion object {
        var currentUser: User? = null
        var todayTime = ArrayList<Int>()
        var morningTime = ArrayList<Int>()
        var afternoonTime = ArrayList<Int>()
        var eveningTime = ArrayList<Int>()
        lateinit var weakReference: WeakReference<HomeActivity>

        fun finishActivity(){
            if (weakReference.get() != null){
                (weakReference.get() as HomeActivity).finish()
            }
        }
    }

    private var viewpagerAdapter = ViewpagerAdapter(supportFragmentManager)
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                viewpager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_booking -> {
                viewpager.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_store -> {
                viewpager.currentItem = 2
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_club -> {
                viewpager.currentItem = 3
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                viewpager.currentItem = 4
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        weakReference = WeakReference<HomeActivity>(this)

        val userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        currentUser = userViewModel.getCurrentUser()
        getTodayAvailability()

        qr_btn.setOnClickListener {
            val intent = Intent(this, QRActivity::class.java)
            startActivity(intent)
        }

        viewpagerAdapter.addFragment(HomeFragment.newInstance())
        viewpagerAdapter.addFragment(BookingFragment.newInstance())
        viewpagerAdapter.addFragment(RootFragment.newInstance())
        viewpagerAdapter.addFragment(ClubFragment.newInstance())
        viewpagerAdapter.addFragment(AccountFragment.newInstance())
        viewpager.adapter = viewpagerAdapter
        viewpager.offscreenPageLimit = 2
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {
                navigation.menu.getItem(p0).isChecked = true
            }
        })
    }

    override fun switchToProfile(index: Int) {
        AccountFragment.getExpandedIndex(index)
        viewpager.currentItem = 4
    }

    override fun switchToBooking(time: Int) {
        val fragment = viewpagerAdapter.fragmentList[1] as BookingFragment
        fragment.getSelectedTime(time)
        viewpager.currentItem = 1
    }

    private fun getTodayAvailability(){
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        format.isLenient = false
        val today = format.format(Date())
        val header = mapOf("Content-Type" to "application/jason")
        val body = JSONObject()
        body.put("bookingDate", today)
        OkHttpUtil.sendHttpRequestUseOkHttp(
            ACE_URL + AVAILABILITY_PREFIX_URL, "POST", header, body,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    Snackbar.make(home_root, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
                        val data = jsonObjectResponse.getJSONArray("data")
                        if (data != null) {
                            for (i in 0 until data.length()) {
                                val interval = data.getInt(i)
                                todayTime.add(interval)
                                when (TimeUtil.getTimePeriod(interval)) {
                                    TimeUtil.MORNING -> morningTime.add(interval)
                                    TimeUtil.AFTERNOON -> afternoonTime.add(interval)
                                    TimeUtil.EVENING -> eveningTime.add(interval)
                                }
                            }
                            val intent = Intent("TODAY_AVAILABILITY")
                            LocalBroadcastManager.getInstance(this@HomeActivity).sendBroadcast(intent)
                        }
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        Snackbar.make(home_root, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }
}