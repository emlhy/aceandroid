package com.hongyi.aceandroid.Adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.currentUser
import com.hongyi.aceandroid.Activities.SignInActivity
import com.hongyi.aceandroid.Fragments.WalletFragment
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.ACE_URL
import com.hongyi.aceandroid.Utils.OkHttpUtil
import com.hongyi.aceandroid.Utils.PURCHASE_MEMBERSHIP_PREFIX_URL
import com.hongyi.aceandroid.Utils.TOKEN_CARD_PREFIX_URL
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.lang.ref.WeakReference

/**
 * Created by Hongyi on 2019-02-07.
 * @author Hongyi
 */

class DetailCardAdapter(private val context: Context, private val fragmentContext: WalletFragment, private val jsonArray: JSONArray, private val type: Int) : RecyclerView.Adapter<DetailCardAdapter.ViewHolder>() {
    private val MEMBERSHIP_TYPE = 1
    private val TOKEN_TYPE = 2
    private val BALANCE_TYPE = 3

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DetailCardAdapter.ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_detail_card, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return jsonArray.length()
    }

    override fun onBindViewHolder(p0: DetailCardAdapter.ViewHolder, p1: Int) {
        val jsonObject = jsonArray.get(p1) as JSONObject
        val price = jsonObject.getInt("price").toString()
        p0.price.text = "$$price"
        when (type){
            MEMBERSHIP_TYPE -> {
                p0.detail.text = jsonObject.getInt("typeLength").toString() + " months"
                p0.card.setOnClickListener {
                    if (currentUser != null) {
                        val builder = AlertDialog.Builder(context)
                        builder.setTitle("Purchase membership")
                        builder.setMessage("Are you sure?")
                        builder.setCancelable(true)
                        builder.setPositiveButton("YES") { dialog, which ->
                            purchaseMembership(jsonObject.getInt("id"))
                        }
                        builder.setNegativeButton("No") { dialog, which -> }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    } else {
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, "Please login before you purchase", Snackbar.LENGTH_LONG)
                            .setAction("OK") {
                                val intent = Intent(context as Activity, SignInActivity::class.java)
                                context.startActivity(intent)
                                context.finish()
                            }.addCallback(object : Snackbar.Callback() {
                                override fun onShown(sb: Snackbar?) {
                                    super.onShown(sb)
                                }

                                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                                    super.onDismissed(transientBottomBar, event)

                                }
                            }).show()
                    }
                }
            }
            TOKEN_TYPE -> {
                p0.detail.text = jsonObject.getInt("tokens").toString() + " tokens"
                p0.card.setOnClickListener {
                    if (currentUser != null) {
                        val builder = AlertDialog.Builder(context)
                        builder.setTitle("Purchase token card")
                        builder.setMessage("Are you sure?")
                        builder.setCancelable(true)
                        builder.setPositiveButton("YES") { dialog, which ->
                            purchaseTokenCard(jsonObject.getInt("id"))
                        }
                        builder.setNegativeButton("No") { dialog, which -> }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    } else {
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, "Please login before you purchase", Snackbar.LENGTH_LONG)
                            .setAction("OK") {
                                val intent = Intent(context as Activity, SignInActivity::class.java)
                                context.startActivity(intent)
                                context.finish()
                            }.addCallback(object : Snackbar.Callback() {
                                override fun onShown(sb: Snackbar?) {
                                    super.onShown(sb)
                                }

                                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                                    super.onDismissed(transientBottomBar, event)

                                }
                            }).show()
                    }
                }
            }
            BALANCE_TYPE -> {
                p0.detail.text = "$$price" + " + " + jsonObject.getInt("bonus").toString()
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var price: TextView = itemView.findViewById(R.id.price)
        var detail: TextView = itemView.findViewById(R.id.detail)
        var card: CardView = itemView.findViewById(R.id.detail_card)
    }

    private fun purchaseMembership(membershipTypeId: Int){
        val header = mapOf("Content-Type" to "application/jason", "auth-token" to currentUser!!.token)
        val body = JSONObject()
        body.put("membershipTypeID", membershipTypeId)
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + PURCHASE_MEMBERSHIP_PREFIX_URL, "POST", header, body,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
//                        val data = jsonObjectResponse.getJSONObject("data")
                        val message = jsonObjectResponse.getString("msg")
                        val fragmentReference = WeakReference<WalletFragment>(fragmentContext)
                        val walletFragment = fragmentReference.get()
                        (context as Activity).runOnUiThread {
                            walletFragment?.getWalletInfo()
                        }
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }

    private fun purchaseTokenCard(tokenCardTypeId: Int){
        val header = mapOf("Content-Type" to "application/jason", "auth-token" to currentUser!!.token)
        val body = JSONObject()
        body.put("tokenCardTypeId", tokenCardTypeId)
        OkHttpUtil.sendHttpRequestUseOkHttp(ACE_URL + TOKEN_CARD_PREFIX_URL, "POST", header, body,
            object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("1234567890", e.toString())
                    val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                    Snackbar.make(rootView, e.message.toString(), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call, response: Response) {
                    val jsonObjectResponse = JSONObject(response.body()?.string())
                    if (response.isSuccessful) {
//                        val data = jsonObjectResponse.getJSONObject("data")
                        val message = jsonObjectResponse.getString("msg")
                        if (!message.contains("activate your membership")){
                            val fragmentReference = WeakReference<WalletFragment>(fragmentContext)
                            val walletFragment = fragmentReference.get()
                            (context as Activity).runOnUiThread {
                                walletFragment?.getWalletInfo()
                            }
                        }
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    } else {
                        val message = jsonObjectResponse.getString("msg")
                        val rootView: ConstraintLayout = (context as Activity).findViewById(R.id.home_root)
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
                    }
                }
            })
    }
}