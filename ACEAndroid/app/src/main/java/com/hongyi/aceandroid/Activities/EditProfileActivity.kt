package com.hongyi.aceandroid.Activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.widget.ImageView
import com.hongyi.aceandroid.Activities.HomeActivity.Companion.currentUser
import com.hongyi.aceandroid.Databases.Helpers.UserViewModel
import com.hongyi.aceandroid.Entities.Commodity
import com.hongyi.aceandroid.Entities.User
import com.hongyi.aceandroid.R
import com.hongyi.aceandroid.Utils.*
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.choose_image
import kotlinx.android.synthetic.main.activity_edit_profile.edit_confirm_password
import kotlinx.android.synthetic.main.activity_edit_profile.edit_email
import kotlinx.android.synthetic.main.activity_edit_profile.edit_emergency_contact
import kotlinx.android.synthetic.main.activity_edit_profile.edit_emergency_number
import kotlinx.android.synthetic.main.activity_edit_profile.edit_password
import kotlinx.android.synthetic.main.activity_edit_profile.edit_phone_number
import kotlinx.android.synthetic.main.activity_edit_profile.edit_username
import kotlinx.android.synthetic.main.activity_edit_profile.submit_btn
import kotlinx.android.synthetic.main.activity_edit_profile.txt_confirm_password
import kotlinx.android.synthetic.main.activity_edit_profile.txt_email
import kotlinx.android.synthetic.main.activity_edit_profile.txt_emergency_number
import kotlinx.android.synthetic.main.activity_edit_profile.txt_password
import kotlinx.android.synthetic.main.activity_edit_profile.txt_phone_number
import kotlinx.android.synthetic.main.activity_edit_profile.txt_username
import kotlinx.android.synthetic.main.activity_sign_up.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception

/**
 * Created by Hongyi on 2019-04-22.
 * @author Hongyi
 */

class EditProfileActivity : AppCompatActivity() {
    private val REQUEST_CODE_IMAGE = 101
    private val folderPath = Environment.getExternalStorageDirectory().path + File.separator + "ACE"
    private val aceFolder = File(folderPath)
    private val tempFile = File(folderPath + File.separator + "avatar.jpg")
    private var image : Bitmap? = null
    lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        if (currentUser!!.profileLink != "null")
            Picasso.get().load(currentUser!!.profileLink).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(
                NetworkPolicy.NO_CACHE).into(choose_image, object: com.squareup.picasso.Callback {
                override fun onSuccess() {

                }

                override fun onError(e: Exception?) {
                    choose_image.setImageResource(R.mipmap.ic_launcher_round)
                }

            })
        initListener()
    }

    override fun onPause() {
        super.onPause()
        tempFile.delete()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK){
            if (requestCode == REQUEST_CODE_IMAGE){
                lateinit var bitmap : Bitmap
                if (data?.data == null){
                    bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        val uri = FileProvider.getUriForFile(this, "com.hongyi.aceandroid.fileprovider", tempFile)
                        MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                    } else {
                        MediaStore.Images.Media.getBitmap(this.contentResolver, Uri.fromFile(tempFile))
                    }
                    bitmap = ImageUtil.rotateImage(tempFile.path, bitmap)
                } else {
                    val uri = data.data
                    bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                }
                bitmap = ThumbnailUtils.extractThumbnail(bitmap, 512, 512)
                try {
                    val out = FileOutputStream(tempFile)
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
                    out.flush()
                    out.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                image = bitmap
                choose_image.scaleType = ImageView.ScaleType.FIT_CENTER
                choose_image.setImageBitmap(bitmap)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun initListener(){
        submit_btn.setOnClickListener {
            val username = edit_username.text.toString()
            val phoneNumber = edit_phone_number.text.toString()
            val password = edit_password.text.toString()
            val confirmPassword = edit_confirm_password.text.toString()
            val email = edit_email.text.toString()
            val emergencyContact = edit_emergency_contact.text.toString()
            val emergencyNumber = edit_emergency_number.text.toString()
            val image = this.image
            if (phoneNumber.isNotEmpty() && !ValidationUtil.isValidPhoneNumber(phoneNumber)){
                txt_phone_number.error = "This is not a valid phone number"
                txt_phone_number.isErrorEnabled = true
            } else if (password.isNotEmpty() && password != confirmPassword){
                txt_confirm_password.error = "Must match the previous entry"
                txt_confirm_password.isErrorEnabled = true
            } else if (email.isNotEmpty() && !ValidationUtil.isValidEmail(email)){
                txt_email.error = "This is not a valid email"
                txt_email.isErrorEnabled = true
            } else if (emergencyNumber.isNotEmpty() && !ValidationUtil.isValidPhoneNumber(emergencyNumber)) {
                txt_emergency_number.error = "This is not a valid phone number"
                txt_emergency_number.isErrorEnabled = true
            }else {
                if (image != null) {
                    updateAvatar(username, phoneNumber, password, email, emergencyContact, emergencyNumber)
                } else
                    updateProfile(username, phoneNumber, password, email, emergencyContact, emergencyNumber, "")
            }
        }

        choose_image.setOnClickListener {
            if (!aceFolder.exists())
                aceFolder.mkdirs()
            val galleryIntent = Intent(Intent.ACTION_PICK, null)
            galleryIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*")
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (tempFile.exists()){
                tempFile.delete()
                tempFile.createNewFile()
            } else {
                tempFile.createNewFile()
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                cameraIntent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                val uri = FileProvider.getUriForFile(this, "com.hongyi.aceandroid.fileprovider", tempFile)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            } else {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile))
            }
            val chooser = Intent.createChooser(galleryIntent, "Please choose an app for your image")
            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(cameraIntent))
            startActivityForResult(chooser, REQUEST_CODE_IMAGE)
        }

        log_out.setOnClickListener {
            val userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Log out")
            builder.setMessage("Are you sure?")
            builder.setCancelable(true)
            builder.setPositiveButton("YES"){dialog, which ->
                userViewModel.deleteAllUsers()
                currentUser = null
                val intent = Intent(this, SignInActivity::class.java)
                startActivity(intent)
                HomeActivity.finishActivity()
                finish()
            }
            builder.setNegativeButton("No"){dialog, which ->}
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        edit_username.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p3 == 1 || p3 == 0){
                    txt_username.isErrorEnabled = false
                }
            }

        })

        edit_phone_number.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val phoneNumber = edit_phone_number.text.toString()
                if (phoneNumber.isEmpty() || ValidationUtil.isValidPhoneNumber(phoneNumber)){
                    txt_phone_number.isErrorEnabled = false
                } else {
                    txt_phone_number.error = "This is not a valid phone number"
                    txt_phone_number.isErrorEnabled = true
                }
            }

        })

        edit_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p3 == 1 || p3 == 0){
                    txt_password.isErrorEnabled = false
                }
            }

        })

        edit_email.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val email = edit_email.text.toString()
                if (email.isEmpty() || ValidationUtil.isValidEmail(email)){
                    txt_email.isErrorEnabled = false
                } else {
                    txt_email.error = "This is not a valid email"
                    txt_email.isErrorEnabled = true
                }
            }

        })

        edit_confirm_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                if (p3 == 1 || p3 == 0){
//                    txt_email.isErrorEnabled = false
//                }
                val password = edit_password.text.toString()
                val confirmPassword = edit_confirm_password.text.toString()
                if (password == confirmPassword){
                    txt_confirm_password.isErrorEnabled = false
                } else {
                    txt_confirm_password.error = "Must match the previous entry"
                    txt_confirm_password.isErrorEnabled = true
                }
            }

        })

        edit_emergency_number.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val phoneNumber = edit_emergency_number.text.toString()
                if (phoneNumber.isEmpty() || ValidationUtil.isValidPhoneNumber(phoneNumber)){
                    txt_phone_number.isErrorEnabled = false
                } else {
                    txt_phone_number.error = "This is not a valid phone number"
                    txt_phone_number.isErrorEnabled = true
                }
            }

        })
    }

    private fun updateProfile(username: String, phoneNumber: String, password: String, email: String, emergencyContact: String, emergencyNumber: String, profileLink: String){
        val updateHeader = mapOf("Content-Type" to "application/jason", "auth-token" to currentUser!!.token)
        val updateBody = JSONObject()
        if (username.isNotEmpty() || phoneNumber.isNotEmpty() || password.isNotEmpty() || email.isNotEmpty() || emergencyContact.isNotEmpty() || emergencyNumber.isNotEmpty() || profileLink.isNotEmpty()) {
            if (username.isNotEmpty())
                updateBody.put("nickName", username)
            if (phoneNumber.isNotEmpty())
                updateBody.put("phoneNumber", phoneNumber)
            if (password.isNotEmpty())
                updateBody.put("password", password)
            if (email.isNotEmpty())
                updateBody.put("email", email)
            if (emergencyContact.isNotEmpty())
                updateBody.put("emergencyContact", emergencyContact)
            if (emergencyNumber.isNotEmpty())
                updateBody.put("emergencyNumber", emergencyNumber)
            OkHttpUtil.sendHttpRequestUseOkHttp(
                ACE_URL + USER_UPDATE_PREFIX_URL,
                "POST",
                updateHeader,
                updateBody,
                object :
                    okhttp3.Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Snackbar.make(edit_profile_root, e.message.toString(), Snackbar.LENGTH_LONG).show()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        val jsonObjectResponse = JSONObject(response.body()?.string())
                        if (response.isSuccessful){
                            val userData = jsonObjectResponse.getJSONObject("data")
                            val user = User(userData.getString("uuid"),
                                userData.getString("nickName"),
                                userData.getString("phoneNumber"),
                                userData.getString("email"),
                                userData.getString("qrCode"),
                                userData.getInt("role"),
                                userData.getString("profileLink"),
                                currentUser!!.token)
                            userViewModel.updateUser(user)
                            currentUser = null
                            Snackbar.make(edit_profile_root, jsonObjectResponse.getString("msg"), Snackbar.LENGTH_LONG)
                                .show()
                            HomeActivity.finishActivity()
                            finish()
                            val intent = Intent(this@EditProfileActivity, HomeActivity::class.java)
                            startActivity(intent)
                        }
                    }

                })
        }
    }

    private fun updateAvatar(username: String, phoneNumber: String, password: String, email: String, emergencyContact: String, emergencyNumber: String){
        val updateHeader = mapOf("auth-token" to currentUser!!.token)
        OkHttpUtil.uploadImage(ACE_URL + UPDATE_AVATAR_PREFIX_URL, tempFile, updateHeader, object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Snackbar.make(edit_profile_root, e.message.toString(), Snackbar.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call, response: Response) {
                val jsonObjectResponse = JSONObject(response.body()?.string())
                if (response.isSuccessful) {
                    val profileLink = jsonObjectResponse.getString("imageUrl")
                    updateProfile(username, phoneNumber, password, email, emergencyContact, emergencyNumber, profileLink)
                } else {

                }
            }

        })
    }
}